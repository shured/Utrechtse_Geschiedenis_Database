#!/usr/bin/perl
# Vul de Utrecht.db
use warnings;
use strict;
use DBI;

my ( $file );

## Connect to DB
my $dbname="utrecht.db";
my $dbh = DBI->connect("dbi:SQLite:dbname=$dbname","","") or die $DBI::errstr;

my @files = <Agenda_*.txt>;
foreach $file (@files) {
    print "Processing: " . $file . "\n";
    my $year = substr($file, 7,4);
    print "Jaar is " . $year . "\n";
    my $count="select count(jaar) from Agenda where jaar = $year";
    my $sth = $dbh->prepare($count);
    $sth->execute();
    my ($aantal) = $sth->fetchrow();
    print "Huidige aantal is $aantal\n";

    my $delete="delete from Agenda where jaar = $year";
    my $stg = $dbh->prepare($delete);
    $stg->execute();
    my $cmd= `echo ".mode csv" > /tmp/$year.sql`;
    $cmd = `echo ".separator ;" >> /tmp/$year.sql`;
    $cmd = `echo ".import Agenda_$year.txt agenda" >> /tmp/$year.sql`;
    $cmd = `sqlite3 utrecht.db '.read /tmp/$year.sql'`;


    $sth = $dbh->prepare($count);
    $sth->execute();
    $aantal = $sth->fetchrow();
    print "Nieuwe aantal is $aantal\n";
}
