1989;1;1;Het inwonertal van de gemeente Utrecht is in 1988 met 874 personen toegenomen tot 230.372. De gemeente Amersfoort groeide met 2.408 personen tot een inwonertal van 95.924. De provincie telt nu 1.004.632 inwoners. Dat is 38.484 meer dan op 1 januari 1988. Dis is mede het gevolg van de provinciale herindeling.
1989;1;1;In 1988 rukte de brandweer in Utrecht 144 maal minder vaak uit dan in 1987. Namelijk 2.382 keer. Wel bedroeg de schade 20 miljoen gulden meer. Dit is voor een groot deel het gevolg van de grote brand van het Gebouw voor Kunsten en Wetenschappen.
1989;1;1;Het nieuwe muntmeestersteken van de vorig jaar in dienst getreden muntmeester drs. C. van Draanen zal bestaan uit een gestyleerde pijl en boog.
1989;1;1;In het westen van de provincie vormen door de nieuwe indeling 24 oude stadjes en dorpen vanaf vandaag 14 nieuwe gemeenten. Ook de gemeente Woerden behoort nu weer tot Utrecht.
1989;1;1;Het Regionaal Nutsbedrijf Eemland gaat van start. Hierin zijn de gas en centraalantenne bedrijven van Amersfoort, Baarn en Soest verenigd.
1989;1;1;Wegens bezuiningen wordt de kioks van de VVV Utrecht in het winkelcentrum Hoog Catharijne gesloten.
1989;1;2;Naar aanleiding van de verhuizing later in het jaar van het Academisch Ziekenhuis naar de Uithof verschijnt het door Dick Schaap samengestelde boek "Vaarwel Catharijnesingel" met foto's en verhalen over het oude ziekenhuis.
1989;1;8;De Utrechtse Domroep presenteert haar eerste televisieuitzending.
1989;1;11;De publieksprijs van de door de Stichting Orientatie Herinrichting Domplein Utrecht uitgeschreven prijsvraag is gewonnen door het Utrechtse gemeenteraadslid drs ir. K. Metz en prof ir A.J.H. Hak uit Delf met het ontwerp "Binnenstebuiten".
1989;1;12;Het beeldje van een zakkendrager dat 15 jaar geleden van de poortdoor bij Oudegracht 237 in Utrecht werd ontvreemd, wordt overgedragen aan het gemeentelijk bureau Monumenten.
1989;1;19;Mr. Petra van Luyn wordt door de Utrechtse gemeenteraad benoemd tot gemeentelijke ombudsvrouw.
1989;1;19;De commissaris van de koningin jhr. drs. P.A.C. Beelaerts van Blokland opent officieel het nieuwe gebouw van de faculteit Beeldende Kunsten en Vormgeving van de Hogeschool voor de kunsten aan de Ina Boudier-Bakkerlaan in Utrecht.
1989;1;20;Bij zijn afscheid van de Utrechtse Welstandscommissie ontvangt ir. K. van der Gaast uit handen van burgemeester drs. M.W.M. Vos-van Gortel de zilveren stadsmedaille.
1989;1;20;Op een binnenplaats aan de Keukenstraat in Utrecht wordt het door Ina Bolle vervaardigde beeld "Stenen Meisje" onthuld.
1989;1;24;Wethouder C. Pot ontvangt uit handen van ir F. Marckmann, vice-voorzitter van de Utrechtse Klokkenspelvereniging twee klokjes bedoeld voor de uitbreiding van het carillon van de Nicolaaskerk.
1989;1;24;De vereniging voor Kunstzinnige Vorming betrekt haar nieuwe pand Lucas Bolwerk 11 in Utrecht.
1989;1;25;Wethouder drs. R.M. ten Cate-Dhont (VVD) stelt op het terrein bij de Veilinghaven in Utrecht een installatie voor scheiding van baggerslib in gebruik.
1989;1;27;Uit handen van burgemeester Vos van Utrecht ontvangt C. de Vriese, tot 1 januari 1989 algemeen directeur van de Jaarsbeurs, de zilveren stadsmedaille. Hij is opgevolgd door R. van Ingen.
1989;1;31;Mr. E.M. d'Hondt, burgemeester van Maarssen wordt voorzitter van de provinciale vereniging voor Vreemdelingen Verkeer als opvolger van mr G.J. de Graaf.
1989;2;1;Tegelijk met een reoganisatie van de provinciale diensten is het provinciewapen gemoderniseerd en een nieuwe logo voor de provincie ontworpen.
1989;2;2;Ter gelegenheid van het 90-jarig bestaat van de Onderlinge Pharmaceutische Groothandel te Utrecht ontvangt Burgemeester Vos het eerste exemplaar van der herziene druk van het in 1953 verschenen boek Nederlandse Vijzels van dr. D.A. Wittop Koning.
1989;2;3;Mr. ds. L.C. Brinkman, minister van WVC, opent het pand Centrum Maliebaan van de Utrechtse op het gebied van de zorg voor verslaafden.
1989;2;4;Het kerkbestuur van de hervormde kerk in Lage Vuursche presenteert het boek "Van Stulp tot kerk" van Willem Otten over de historie van het bosgebied tussen Gooi, Eemland en Utrechtse Heuvelrug.
1989;2;13;Bij zijn aftreden als voorzitter van de rampencommissie van de Stichting Samenwerkende Utrechtse Ziekenhuizen ontvangt A.M. Stuij de Maartenspenning van de stad Utrecht.
1989;2;15;Burgemeester Vos van Utrecht en J. Jessurun, plaatsvervangend directeur-generaal Culturele Zaken van het ministerie van WVC ontvangen de eerste exemplaren van het door drs. M.C. Dolfin, ir E.M. Kylstra en drs. J. Penders samengestelde boek "Utrecht, de huizen binnen de singels".
1989;2;16;In het Utrechtse Universiteitsmuseum wordt de tentoonstelling "Over magnetiseurs en hypnose" geopend.
1989;2;17;Op het terrein van het medisch centrum Berg en Bosch in Bilthoven wordt het Diabetescentrum Bilthoven geopend door het Tweede Kamerlid dr. A. Lansink.
1989;2;20;In het gerechtsgebouw aan de Hamburgerstraat in Utrecht houdt een man gedurende vier uur een viertal mensen in gijzeling uit onvrede over de bezoekregeling met zijn kinderen.
1989;2;23;Drs. C.P. van Dijk, minister van Binnenlandse Zaken, opent in de Crediet- en Effectenbank aan de Kromme Nieuwegracht in Utrecht de tentoonstelling "175 jaar gemeentepolitie Utrecht".
1989;2;24;In Museum Flehite in Amersfoort wordt de tentoonstelling "Contouren en Contrasten" geopend van foto's van Connie Meslier en Jos Ruysenaars over de Stationsbuurt en over Amersfoort als stad van contrasten.
1989;2;27;Wethouder drs. W.P.C. van Willigenburg (VVD) maakt bekend dat de gemeente Utrecht de bevolking via de media gaat waarschuwen als op bepaalde punten in de stad de lucht zo ernstig vervuild dat de gezondheidszorg in gevaar dreigt te komen.
1989;3;2;In het Utrechts Universiteitsmuseum wordt ter gelegenheid van de 400e geboortedag van prof. dr. Gisbertus Voetius de tentoonstelling "De onbekende Voetius" geopend.
1989;3;3;In de Geertekerk wordt door de Remonstrantse Gemeente Utrecht het gerestaureerde Strumphlerorgel in gebruik genomen.
1989;3;6;In de wijk Oog in ol in Utrecht werpen bewoners blokkades op bij de Central Soya fabriek uit protest tegen de stank- en geluidsoverlast.
1989;3;7;Gedeputeerde T. Poortenaar-Sikkema opent de uitgebreide en gerenoveerde polikliniek van het ziekenhuis De Lichtenberg in Amersfoort.
1989;3;9;Met affiches van Dick Bruna wordt de actie "Samenleving en Criminaliteit" onder de aandacht van het publiek in het winkelcentrum Hoog Catharijne gebracht.
1989;3;10;Mr. E.J.L.M. de Bruijn neemt afscheid als wethouder van Economische Zaken van Amersfoort.
1989;3;10;Het nieuwe opvanghuis voor 40 asielzoekers in het pand Brigittestraat 3 in Utrecht wordt geopend.
1989;3;13;Wethouder M.M. Andriessen-Brockman van Volkshuisvesting slaat de eerste paal voor 106 seniorenwoningen in de Utrechtse wijk Lunetten.
1989;3;13;Burgemeester Vos installeert 12 toezichthouders voor het winkelcentrum Hoog Catharijne.
1989;3;15;Culinair specialiste Wina Born opent in Huize Doorn de tentoonstelling "Keizerlijke Menu's".
1989;3;16;De gemeenteraad van Utrecht stemt in met het besluit het Vinkepand op het Vredenburg in Utrecht in erfpacht uit te geven aan de firma Staffhorst.
1989;3;17;In het Centraal Museum in Utrecht wordt door de cultureel attache van Polen, Jan Larecki, de tentoonstelling "Zeventiende eeuwse meesterwerken uit het National Museum te Warschau" geopend.
1989;3;18;In Slot Zeist wordt de tentoonstelling "Licht en Lood" geopend.
1989;3;20;Wethouder Van Willigenburg (VVD) ontvangt het eerste exemplaar van het boek "Utrechtse stadstrams en stadsbussen op oude ansichten", samengesteld door Max Veldhuis.
1989;3;22;In de Kloosterhof van de Domkerk wordt het beeld van Jan van der Doem herplaatst.
1989;3;22;Wethouder Willigenburg (VVD) pompt het eerste water uit de enige maanden geleden gelaatste replica van de oude waterpomp op de Breedstraat in Utrecht.
1989;3;23;Burgemeester A. Schreuder van Amersfoort geeft het startsein voor de bouw van het nieuwe onderkomen voor de Evangelische Hogeschool aan de Puntenburglaan.
1989;3;29;Drs. E. Heerma, staatssecretaris van Volkshuisvesting, Ruimtelijke Ordening en Milieubeheer, geeft het startsein voor de woningbouw op het tien jaar geleden vrij gekomen terrein van de faculteit der Diergeneeskunde op de Alexander Numandkade in Utrecht.
1989;3;31;Dr. F.J. Grommers, docent aan de faculteit der Diergeneeskunde van de Rijksuniversiteit Utrecht, ontvangt voor zijn kritische houding ten opzichte van de omgang met landbouwhuisdieren en zijn afwijzing van biogenetische manipulatie de Goude Giraf, de wisselprijs van de afdeling Utrecht van de Dierenbescherming.
1989;4;3;Ter gelegenheid van de 353e Dies Natalis van de Rijksuniversiteit Utrecht ontvangen prof. dr. V.I.Arnol'd uit Moskou en prof. dr. L.W. Hall uit Cambrige eredoctoraten.
1989;4;3;In Utrecht wordt gestart met een project dagopvang voor demente bejaarden voor wie nog geen plaats in een verpleeghuis vrij is.
1989;4;4;Mr. F.B. Keulen ontvangt uit handen van burgemeester Vos de zilveren stadsmedaille. Hij verdedigt al vele jaren de Utrechtse belangen voor de rechter.
1989;4;5;De directies en besturen van het St. Elisabethziekenhuis en ziekenhuis De Lichtenberg ondertekenen in de Celzusterkamer in het hofje Armen de Poth in Amersfoort de statuten van de Stichting Algemeen Christelijk Ziekenhuis Eemland. Dit is de eerste officiele stap om te komen tot 1 ziekenhuis in Amersfoort.
1989;4;7;De commissaris van de koningin jhr. Beelaerts van Blokland opent het nieuwe pand van de Stichting Vredeopbouw aan de Oosterkade in Utrecht.
1989;4;8;Milieugedeputeerde Ir. G.C. van Wijnbergen stelt de recyclingapparatuur voor bouwafval op het industrieterrein Lage Weide in Utrecht in gebruikt.
1989;4;11;Burgemeester Vos ontvangt het eerste exemplaar van het door drs. M. Donkersloot-de Vrij geschreven boeken "Kaarten van Utrecht".
1989;4;12;In de nacht wordt het borstbeeld van burgemeester dr. B. Reiger van de Maliebaan in Utrecht gestolen door krakers, die hiermee de ontruiming van door hen gekraakte panden willen voorkomen.
1989;4;14;Het al enige maanden werkende Buro Slachtofferhulp in Utrecht wordt officieel geopend.
1989;4;14;In het museum Flehite in Amersfoort wordt de tentoonstelling "Gedrukt voor het leven - drukwerk van de wieg tot het graf" geopend.
1989;4;15;Ir. E.J.G. Scheffer uit Bilthoven ontvang uit handen van wethouder Pot de Maartenspenning van de stad Utrecht voor het vele werk dat hij heeft gedaan voor de Muziekbibliotheek van Utrecht.
1989;4;15;De oud-katholieke St. Gertrudiskerk aan het Willemsplantsoen in Utrecht wordt na restauratie weer in gebruik genomen.
1989;4;16;In de Utrechte Jaarbeur wordt het door Veilig Verkeer Nederland georganiseerde Nationale Verkeersspektakel gehouden.
1989;4;18;In Utrecht overlijdt op 63-jarige leeftijd Joop de Winter, de kastelijn van het overbekende cafe Dikke Dries in Wijk C.
1989;4;20;Bekend wordt dat deze week het aantal inwoners van de provincie Utrecht het miljoen heeft overschreven.
1989;4;21;De Nieuwegeinse wethouder drs. T.J. Buursema opent op Fort Vreeswijk de Oudheidskamer Vreeswijk.
1989;4;28;De Rijksuniversiteit Utrecht draagt het landgoed Sandwijck in De Bilt over aan de stichting Het Utrechts Landschap, die het daarna voor het symbolische bedrag van 1 gulden verkoopt aan de woningstichting Lieven de Key, die het zal verbouwen tot appartementen.
1989;4;28;Burgemeester Vos opent de nieuwe hal van het Centraal Station in Utrecht.
1989;5;8;In het vervolg wordt een gedeelte van het winkelcentrum Hoog Catharijne in Utrecht gedurende de nacht afgesloten in de hoop dat hierdoor de Criminaliteit zal afnemen.
1989;5;9;Drs. N. Smit-Kroes, minister van Verkeer en Waterstaat, installeert het Regioneel Orgaan Verkeersveiligheid Utrecht.
1989;5;10;Wethouder Ten Cate (VVD) stelt de grootste gasturbine ter wereld in werking op het industrieterrein Lage Weide in Utrecht.
1989;5;10;Door een vertegenwoordiger van Europe Nostra, een overkoepelende organisatie van particuliere organisaties op het terrein van monumentenzorg, wordt in Slot Zeist een Diploma of Merit uitgereikt voor de restauratie van het langoed Ma Retraite in Zeist.
1989;5;12;Burgemeester Vos opent de vernieuwde Hortus Botanicus van de Rijksuniversiteit Utrecht in de Uithof.
1989;5;12;In Werkhoven wordt het eerste deel van zeven toeristische monumentenroute gidsen, uitgegeven door de provinciale VVV, gepresenteerd. Dit eerste deel bestrijkt het Kromme Rijngebied.
1989;5;12;Wethouder F.H. van der Sluijs van Volksgezondheid verricht de officiele opening van het al sinds februari in gebruik zijnde gebouw van het Jongeren Advies Centrum aan de Plompetorengracht in Utrecht.
1989;5;17;In Paushuize reikt de commissaris van de koningin provinciale legpenningen uit aan de Nederlandse Spoorwegen, die dit jaar 150 jaar bestaat en 1 van de belangrijkste werkgevers in de provincie zijn, en aan jhr. mr. K. Bosch ridder van Rosenthal, de scheiden voorzitter van het recreatieschap Vinkeveense Plassen.
1989;5;17;Ter gelegenheid van het 350-jarig bestaan van de Doopsgezinde Gemeente in Utrecht wordt in de kerk de tentoonstelling "De man van Nazareth geopend".
1989;5;19;Wethouder Willigenburg (VVD) draagt de gerestaureerde theekoepel aan de Vechtdijk in Utrecht over aan het verzorginstehuis 't Huis aan de Vecht.
1989;5;19;In de gemeente Leusden wordt een beeldenroute geopend.
1989;5;19;Prof. dr. Lucas Reijnders opent de in en rond Kasteel Groeneveld in Baarn opgericht tentoonstelling "Dichter en Landschap".
1989;5;19;Wethouder Pot stelt het geautomatiseerde zoeksysteem voor de archieven van de Utrechtse notarissen over de perioode 1560 tot 1811 van de Gemeentelijke Archiefdienst in werking.
1989;5;20;In het Centraal Museum in Utrecht wordt de tentoonstelling "Groeten uit Utrecht" geopend.
1989;5;23;Drs. R.P.J. van Tetterode, directeur van het Gemeentelijk Vervoerbedrijf Utrecht, introduceert de nieuwe gelede stadsbussen die vanaf 15 juli op verschillende drukke lijnen zullen worden ingezet.
1989;5;24;Drs. W.J. Deetman, minister van Onderwijs, slaat de eerste paal vor de bouw van het laatste gedeelte van de medische faculteit van de Rijksuniversiteit Utrecht in de Uithof.
1989;5;25;Burgemeester Vos stelt bij drukkerij Van Boekhoeven-Bosch de geautomatiseerde drukstraat "Teleprint" in werking. Hier worden alle Nederlandse telefoonboeken gedrukt.
1989;5;27;De gerestaureerde theekoepel 't Oog in 't Zeil aan de Vaarterijn in Utrecht wordt door wethouder Van Willigenburg (VVD) officieel geopend.
1989;5;27;De beeldhouwer René van Seumeren is in Toulouse overleden. In Utrecht staan oa nog beelden van hem bij de Bartholomeusbrug, de Mariaschool en de Beeldengroep Zorg voor het gezin op gebouw van de voormalige levensverzekerings­maatschappij Concordia op de Oudenoord.
1989;5;27;Pierre Janssen opent in het Museum Hedendaagse Kunst in Utrecht de tentoonstelling "XX Utrechtse Signalementen 1986-1989". DIt is de laatste tentoonstelling in dit museum dat op 1 januari 1990 gesloten wordt.
1989;5;30;Drs. J. de Koning, minister van Sociale Zaken, opent aan de Grebbeberglaan in Utrecht het nieuwe pand van het Centrum Beroepsorientatie en Beroepsoefening.
1989;6;1;De voorzitter van het College van Kerkvoogden van het Hervormde Gemeente in Utrecht, P. van Beusichem, ontvant voor de restauratie van de vijf middeleeuse kerken in de Utrechtse binnenstad de Europa Nostra Award 1988.
1989;6;1;De commissaris van de koningin stelt het centrale alarmnummer 06-11 in de regio Utrecht in gebruik.
1989;6;1;Wethouder Ten Cathe-Dhont opent de nieuwe huisvesting van de Gemeentelijke Sociale Dienst Utrecht aan de Oudenoord.
1989;6;1;Gedeputeerde J. Hoekstra opent in het Utrechts Universiteitsmuseum de tentoonstelling "Voortuin van de Wetenschap, 350 jaar Botanische Tuinen in Utrecht".
1989;6;2;Architect ir. T. van Hoogevest reikt de jaarlijkse Archinormprijs uit aan drs. ing. V.P.A. Lukkien, directeur van de Botanische Tuinen in Utrecht, voor de aanleg en indeling van de systeemtuin.
1989;6;6;Mr. Pieter van Vollenhove opent het vernieuwde Nederlands Spoorwegmuseum in Utrecht.
1989;6;7;Wethouder Andriessen-Brockman ontvant uit handen van staatssecretaris Heerma de aan de gemeente Utrecht toegekende Gewoon Veilig Prijs voor het, door de Stichting Vrouwen Bouwen en Wonen, opgestelde plan om de sociale veiligeheid in Utrecht-West te vergroten.
1989;6;7;De Utrechtse gemeenteraad neemt het besluit om in het vervolg de grond in de gemeente in eeuwigdurende erfpacht uit te geven.
1989;6;8;De Utrechtse beeldhouwster Marie-Jose Wessels ontvangt uit handen van Pieter d'Hont de naar hem genoemde aanmoedigingsprijs.
1989;6;15;Bij 's Rijks Munt worden voor het eerst sinds 1816 weer zilveren dukaten geslagen. Dit zijn verzamelaarsmunten die op het initiatief van Tweede Kamerlid mr. Th. H. Joekes weer in de muntwet zijn opgenomen.
1989;6;20;Wethouder Andriessen-Brockman van Volkshuisvesting opent in de Janskerk in Utrecht een tentoonstelling van plannen die drie architechten hebben gemaakt voor het gebied rond de Walsteeg.
1989;6;21;In aanwezigheid van koningin Beatrix en prins Claus wordt op het Centraal Station en in de Jaarbeurshallen de tentoonstelling ter gelegenheid van het 150-jarig bestaan van de Nederlandse Spoorwegen geoped, getiteld "Treinen door de tijd".
1989;6;22;Minister Deetman start officieel de herbouw van het vorig jaar afgebrande Gebouw voor Kunsten en Wetenschappen. Na de herbouw zal het Utrechtse Conservatorium hierin weer gehuisvest worden.
1989;6;22;In fort Rhijnauwen wordt het ontwikkelplan voor het toeristisch aantrekkelijk maken van de Nieuwe Hollandse Waterlinie gepresenteerd.
1989;6;23;Wethouder Van Willigenburg (VVD) neemt in de Gemeentelijk Muziekschool aan het Domplein in Utrecht het eerste exemplaar in ontvangt van het door drs. Anneke van Vlierden geschreven boekje "Het Utrechtse huis De Rode Poort en zijn piscina".
1989;6;24;In museum Flehite in Amersfoort wordt de tentoonstelling "Jukeboxen en speelautomatenparade" geopend.
1989;6;26;Het Utrechts Landschap verwelkomt de 10.000e beschermer.
1989;6;30;Gedeputeerde Van Wijnbergen voor Toerisme en Recreatie ontvangt het eerste exemplaar van deel 1 van de regiogids toeristisch Utrecht in woord en beeld: Utrechtse Heuvelrug, Vallei en Eemland, samengesteld door Reina van der Wal.
1989;6;30;Met de ondertekening van de Acte van Overdracht komt er een einde aan het zelfstandig bestaan van het Ooglijders Gasthuis. Het gaat verder als de Divisie Oogheelkunde van het Academisch Ziekenhuis Utrecht, het F.C. Donders Instituut voor Oogheelkunde.
1989;7;1;Barend de Ronden treedt in dienst als Directeur van de Regionale Omroep Utrecht.
1989;7;1;Dr. M.A. Vente draagt een 17-eeuws transeptorgel, gebouwd door Apollonius Bosch, over aan het Citypastoraat en neemt afscheid als orgeladviseur van de Hervormde Gemeente Utrecht.
1989;7;7;In de vroege uren verhuizen de eerste patienten van het Academisch Ziekenhuis Utrecht van de Catharijnesingel naar het nieuwe gebouw in de Uithof.
1989;7;8;In Slot Zuylen wordt de tentoonstelling "Van Roemers en Bokalen" geopend met glaswerk uit 3 Utrechte kastelen.
1989;7;13;In Utrecht overlijdt op 74-jarige leeftijd dr. M.A. Vente, internationaal vermaard orgeldeskundige.
1989;7;13;Op de Stichte Rotonde bij Amersfoort wordt een staalsculptuur van de Haagse kunstenaar Ernst Hazenbroek onthuld.
1989;7;19;Gedurende twee maanden zullen in Utrecht Mobiele Informatieverstrekkers van de VVV rondlopen.
1989;7;20;Wethouder Van Willigenburg (VVD) onthult aan de gevel van het pand Mariaplaats 17 in Utrecht een door J.A. Tolboom gebeeldhouwde kop van Floris de Zwarte die in 1133 Utrecht onveilig maakte en zich verschanste in de Mariakerk
1989;7;21;Bekend wordt dat onder de Oudegracht in Utrecht bij het voormalige NV huis een 16-eeuws riool, een zogenaamde slokop, is ontdekt.
1989;7;24;De Hogeschool Utrecht koopt voor haar laboratoriumopleidingen het gebouw van het Ooglijders Gasthuis aan de F.C. Dondersstraat in Utrecht.
1989;7;28;In het gebouw van de Crediet- en Effectenbank, Kromme Nieuwegracht in Utrecht wordt een tentoonstelling geopend van kunstwerken uit particulier bezit die te maken hebben met het thema trein, getiteld "Naar spoor gespeurd".
1989;7;29;Het oudste dienstencentrum voor zelfstandig wonende ouderen van Nederland, het Bernadottecentrum in Utrecht, viert het 25-jarig bestaan.
1989;8;5;Bioscoop Scala aan de Potterstraat in Utrecht moet wegens het plotseling ontstaan van scheuren in de projectiecabine halverwege een voorstelling worden gesloten.
1989;8;5;In Austerlitz wordt een uit 1798 daterende waterput weer uitgegraven en opgemetseld.
1989;8;13;Op 78-jarige leeftijd overlijdt in Utrecht G. Pest, beter bekend als goochelaar mijnheer Boeda. Hij heeft verscheidene onderscheidingen ontvangen voor het werk dat hij deed voor het Rode Kruis en het Kanker Instituut.
1989;8;25;In het Rijksmuseum Het Catharijneconvent wordt door de kerkhistoricus prof. dr. J. Visser de tentoonstelling "Kunst uit Oud-Katholieke kerken" geopend.
1989;9;1;A. Beusink volgt M. Faassen op als directeur van de Provinciale VVV.
1989;9;1;Wethouder Van Willigenburg (VVD) verricht de officiele opening van het gerestaureerde Vleeshuis aan de Voorstraat in Utrecht.
1989;9;5;Aan de Van Asch van Wijckskade in Utrecht start de Universiteit voor Humanistiek, die studenten opleidt tot geestelijke raadslieden met een humanistische opvatting.
1989;9;6;Bekend wordt dat bij de restauratie van het Gebouw voor Kunsten en Wetenschappen in Utrecht resten gevonden zijn van de verdwenen Mariakerk.
1989;9;9;In het gebouw van de Gemeentelijke Archiefdienst wordt de tentoonstelling "De Weduwe met de baard, de firma Wed. J.R. van Rossum 1797-1977" over de gelijknamige Utrechtse boekhandel, geopend.
1989;9;9;Ter gelegenheid van de jaarlijkse Nationale Open Monumentendag worden op het bolwerk Zonnenburg in Utrecht een gevelsteen en een informatiepaneel van de ANWB onthuld. Ook Sterrenburg en Manenburg krijgen een informatiepaneel.
1989;9;9;Wethouder Van Willigenburg (VVD) onthult op de Nieuwegracht in Utrecht een brandbel, ruim een halve eeuw nadat de laatste uit het straatbeeld is verdwenen.
1989;9;10;Ter gelegenheid van het 200-jarig bestaan van de Nederlandse Israelitische Gemeente Utrecht ontvangt burgemeester Vos het eerste exemplaar van "De synagogen in Utrecht", een heruitgave van een in 1926 door Jac. Zwarts geschreven boekje.
1989;9;11;In Groenekan dringt er benzine in het waterleidingnet waardoor het drinkwater ernstig verontreinigt wordt.
1989;9;15;In de Pandhof aan het Domplein in Utrecht wordt de tentoonstelling "Beeldhouwers van de Dom" geopend, waar beeldhouwers die betrokken waren bij de restauratie van de Domkerk hun methoden en technieken laten zien.
1989;9;16;Wethouder Pot heropent de geheel opgeknapte sporthal De Dreef in de Utrechtse wijk Overvecht.
1989;9;17;Nadat door een uitspraak van de Raad van State de krakers in het pand aan de Burgemeester Reigerstraat in Utrecht mogen blijven, wordt het in april ontvoerde beeld van burgemeester Reiger terug gegeven.
1989;9;18;In het St. Anthonius Ziekenhuis in Nieuwegein wordt de eerste longtransplantatie in Nederland uitgevoerd.
1989;9;19;In Den Haag overlijdt op 81-jarige leeftijd de beeldhouwster Corinne Franzen-Heslenfeld, de maakster van het verzetsmonument op het Utrechtse Domplein.
1989;9;21;Wethouder Van Willigenburg (VVD) ontvangt het eerste exemplaar van het boekje "Het verhaal van een park dat nog geen park is", samengesteld door de Open Werkgroep Grifpark in Utrecht.
1989;9;22;In het winkelcentrum op de 't Goylaan wordt door burgemeester Vos de eerste Utrechtse politieshop geopend, waar allerlei informatie over de politie verkregen kan worden.
1989;9;27;Met de uitgifte van de eerste Gildegids gaat de Stichting 't Gilde in Utrecht officieel van start. Deze stichting bemiddelt bij de overdracht van kennis, kunde en ervaring door ouderen aan anderen.
1989;9;29;Koningin Beatrix verricht de officiele opening van het Academisch Ziekenhuis Utrecht.
1989;10;2;Staatssecretaris D.Y.W. de Graaf-Nauta van Binnenlandse Zaken installeert het Samenwerkinsverband Midden- en West-Utrecht waarin de stad Utrecht en 15 regiogemeentes op bestuurlijk gebied samenwerken.
1989;10;2;Drs. D.J.D. Dees, staatssecretaris van Volksgezondheid, opent het hoofdkantoor van de Stichting Maatschappeljke Dienstverlening aan Doven in Utrecht.
1989;10;4;De commissaris van de koningin jhr. Beelaerts van Blokland ontvangt uit handen van de samensteller dr. T. van Veen het eerste exemplaar van het boek "Taal en leven in de Vechtstreek" over de dialecten in dit gebied.
1989;10;4;Ter gelegenheid van de start van de Kinderboekenweek 1989 in Utrecht reikt de rector magnificus prof. dr. J.A. van Ginkel de gouden en zilveren Griffels en Penselen uit. In Galerie De Reiger wordt een tentoonstelling geopend van origineel werk van illustratoren van kinderboeken en er is in de Janskerk een groete Uitvindermarkt voor kinderen.
1989;10;4;Door drie met bleoemen versierde autobussen wordt de busbaan langs de rijksweg A2 tussen verkeersplein Ouderijn en de afslag Nieuwegein/IJsselstein officieel in gebruik genomen.
1989;10;7;Bij het 100-jarig bestaan van de sportvereniging Attila ontvangt de voorzitter de erepenning van de gemeente Utrecht.
1989;10;16;Gedeputeerde J. Hoekstra overhandigt aan W. Hol, directrice van de Openbare Bibliotheek van Utrecht, de eerste Innovatieprijs voor Welzijnsinstellingen van de provincie voor het project "Reizend literaire tentoonstellingen voor zes taalgroepen".
1989;10;17;Staatssecretaris van Defensie, J. van Houwelingen, opent de nieuwe Knoopkazerne aan de Mineurslaan in Utrecht. Burgemeester Vos ontvangt het eerste exemplaar van het door de sectie militaire geschiedenis van de landmacht samengestelde boekje "Utrecht als militaire stad".
1989;10;20;Wethouder Van Willigenburg (VVD) onthult aan het pand Oudegracht 293 in Utrecht een gevelsteen voorstellende een helm, met als onderschrift "Den IJzeren Hoet". Dit is de eerste gevelsteen in het kader van het herstelplan voor gevelstekens in de historische binnenstad.
1989;10;20;Op twee achtereenvolgede weekeinden is in Utrecht een atelierroute uitgezet langs 70 ateliers van beeldende kunstenaars.
1989;10;20;Aartsbisschop Simonis opent de tentoonstelling "Leusden 1212" ingericht door de Historische Kring Leusden ter gelegenheid van het feit dat Leusden 1212 jaar geleden voor het eerst werd genoemd.
1989;10;24;het Von Gimborn Arboretum in Doorn, een onderdeel van de Botantische Tuinen van de Rijksuniversiteit Utrecht, ontvangt de International Dendrology Society de Conservation Award.
1989;10;25;Burgemeester Vos opent het geheel gerestaureerde Vinkepand aan het Vredenburg in Utrecht, waar nu Muziek Staffhorst in gevestigd is.
1989;10;28;Ter gelegenheid van het 175-jarig bestaan van de Koninklijke Marechaussee in Nederland houdt de afdeling Utrecht een open dag in Fort De Bilt.
1989;11;2;Bij haar afscheid als wethouder voor Bedrijven, Woningbedrijf en Sociale Zaken ontvangt drs. R.M. ten Cate-Dhont (VVD)  uit handen van burgemeester Vos de zilveren stadsmedaille en de Raadspenning van de stad Utrecht. Zij wordt opgevolgd door mr. A.J. Kokshoorn.
1989;11;2;De secretaris van de Utrechtse Universiteit, mr. W.G. van der Putten, opent in het Universiteitsmuseum de tentoonstellingen "150 jaar fotografie" en "Fotografie in dienst van de wetenschap".
1989;11;4;In Velsen overlijdt de oud-burgemeester van Zuilen (1931-1935) en oud-stadscommandant van de Binnenlandse Strijdkrachten in Utrecht, Menzo Kwint.
1989;11;4;In kasteel Groeneveld in Baarn wordt de tentoonstelling "Nederland, oh Nederland" over de ontstaandsgeschiedenis van het Nederlandse cultuurlandschap geopend.
1989;11;7;De commissaris van de koningin jhr. Beelaerts van Blokland ontvangt uit handen van drs. P.W. Geudeke, directeur van de Topografische Dienst, het eerste exemplaar van de Topografische Atlas van Utrecht.
1989;11;8;Gemeentearchivaris drs . I.W.K.A. Caminada overhandigt aan burgemeester Vos het eerste exemplaar van de eerste aflevering van "Als de dag van gsiteren, honderd jaar Utrecht en de Utrechters".
1989;11;9;Vier leden van het medische team dat sinds 1984 zorgt voor hartoperaties bij Poolse kinderen ontvangt in Warschau de Orde van Verdienste van de Poolse Volksrepubliek.
1989;11;10;Wethouder Van der Sluijs opent het nieuwe buurthuis voor de Zuidelijke binnenstad aan de Oudegracht in Utrecht.
1989;11;15;Door Broese-Kemink wordt de door Wim van Beusekom en Jo van Staveren samengestelde dichtbunde "O, Utrecht, lieve stad", uitgegeven.
1989;11;16;De minister van Volkshuisvesting, Ruimtelijke Ordening en Milieubeheer, J.G.M. Alders, opent samen met wethouder drs. J. van Lidth de Jeude het Service Centrum Grondreiniging in Utrecht door het planten van een koningslinde in thermisch gereinigde grond.
1989;11;16;Als opvolgder van de naar Arnhem vertrokken mr . P. Scholten wordt de uit Best afkomstig J. de Widt benoemd tot burgemeester van Soest.
1989;11;16;Het terrein van het Academisch Ziekenhuis Utrecht aan de Catharijnesingel wordt officieel overgedragen aan de gemeente Utrecht. De naam van het terrein waarop woningen en kantoren worden gebouwd wordt Hooch Boulandt.
1989;11;17;In het Centraal Museum in Utrecht wordt de tentoonstelling "Armando, vier reeksen uit de Berlijnse jaren" geopend.
1989;11;27;Wethouder Van Willigenburg (VVD) sluit de laatste van de woonboten in Utrecht aan op het rioleringsnet.
1989;11;28;Wethouder Van der Sluijs opent een nieuwe afdeling in het bejaardencentrum Alphons Ariens in Utrecht, bestemd voor licht demente ouderen.
1989;11;29;De enige nachtclub van Utrecht, Limburgia, aan de Lange Elisabethstraat gaat dicht.
1989;11;29;De woningcorporatie Stichting Studenten Huisvesting start met het opknappen van de duizendste studentenwoning.
1989;11;30;Het voormalige herstellingsoord voor diaconessen, Oranjestein in Amerongen, wordt verkocht aan een projectontwikkelaar die er appartementen in wil bouwen.
1989;12;1;Bij Broese-Kemink verschijnt ht boek "Het Romeinse Castellum te Utrecht", dat gebaseerd is op archeologisch onderzoek dat prof. dr. A.E. van Giffen tussen 1936 en 1949 op het Domplein verrichtte.
1989;12;1;Dr. A.J. Roobeek wordt de eerste vrouwelijke hoogleraar aan de Universiteit Nijenrode. Zij wordt bijzonder hoogleraar technologie en economie.
1989;12;1;Gedeputeerde ir. G.C. van Wijnbergen verlaat Gedeputeerde Staten om voorzitter te worden van het Zuiveringsschap Limburg. Hij zal worden opgevolgd door P.R. Smink.
1989;12;2;Met een lustrumconcert in Muziekcentrum Vredenburg viert de Landelijke Stichting voor Blinden en Slechtzienden, die gevestigd is aan de Nieuwegracht in Utrecht, haar 125-jarig bestaan.
1989;12;6;Wethouder F. Asselbergs ontvangt het eerste exemplaar van het boekje "Aldegonden, een begrip", geschreven door oud-kraker Kees Huybrechts over de kraakperiode 1976-1980 van het voormalige weeshuis in Amersfoort.
1989;12;9;Prinses Margriet opent in Rijksmuseum Het Catharijneconvent de tentoonstelling "middeleeuse miniaturen uit de Noordelijke Nederlanden".
1989;12;15;De wethouder van Ruimtelijke Ordening, A. van Hassel, verricht de officiele opening van het Bastionhotel aan de Mauritiuslaan in Utrecht.
1989;12;18;Het Gemeenschappelijke Administratie Kantoor en de Gemeenschappelijke Medische Dienst betrekken het nieuwe pand aan de Utrechtse Moeder Teresalaan.
1989;12;20;De gemeente Amersfoort heeft weer een eigen brouwerij. Het bier wordt onder de naam Drie Ringenbier op de markt gebracht
1989;12;20;In Bartimeushage, centrum voor visueel gehandicapten te Zeist, neemt prof. mr. I.A. Diepenhorst afscheid als voorzitter van het bestuur van de vereniging Bartimeus. Hij wordt opgevolgd door oud-minister drs. J. de Koning
1989;12;21;Wethouder Andriessen-Brockman verricht officieel de oplevering van de eerste 20 woningen van het renovatieproject De Ruyche Roobol aan de Nicolaas Ruychaverstraat in Utrecht.
1989;12;22;Burgemeester Vos neemt het portret in ontvangst dat Ton Diekstra heeft gemaakt van de leden van het bestuur van de stichting tot Renovatie en Behoud van Oudaen en dat is bedoeld voor het stadskasteel Oudaen aan de Oudegracht in Utrecht.
1989;12;24;In Utrecht overlijdt op 87-jarige leeftijd George Oversteegen, oprichter, voorzitter en secretaris van de Bond van Wereldburgers.
