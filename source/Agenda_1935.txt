1935;1;2;De werkzaamheden voor de uitbreiding van het Julianapark beginnen.
1935;1;10;Mr. J. Hamburger, directeur van de NV Koninklijke Lood- en zinkpletterijen voorheen A.D. Hamburger, is overleden.
1935;1;11;Burgemeester en Wethouders besluiten tot de vorming van een commissie tot het installen van een onderzoek naar de voedingstoestand van verschillende bevolkingsgroepen.
1935;1;12;J.A.H. Wagenaar, stadsklokkenist, is gehuldigd ter gelegenheid van zijn 40-jarige werkzaamheid als zodanig.
1935;1;16;Dr. R. Josselin de Jong, hoogleraar an de Rijksuniversiteit, houdt zijn afscheidscollege.
1935;2;4;Mr. Is. H. Casuto, hoogleraar aan de Rijksuniversiteit, houdt zijn inaugureele rede.
1935;2;6;W.H.M.J. Smulders, oud-directeur van de Machinefabriek, is overleden.
1935;2;15;Dr. Coenraad Alexander Verrijn Stuart, hoogleraar aan de Rijksuniversiteit, houdt zijn afscheidscollege.
1935;2;21;De Gemeenteraad stelt een regeling vast ten aanzien van het autoverhuurbedrijf.
1935;2;21;De Gemeenteraad besluit medewerking te verlenen tot uitvoering van de plannen tot verbetering van de spoorwegtoestanden.
1935;2;25;Jhr. Mr. W.A. Calkoen, oud-officier van Justitie, is overleden.
1935;3;4;Ir. J.B. van de Werfhorst, hoogleraar vaan de Rijksuniversiteit, hodut zijn inaugureele rede.
1935;3;4;A. Hoogendijk, lid van de Provinciale Staten, is in Zeist overleden.
1935;3;11;Dr. A. Massink, directeur van het Rijkslaboratorium voor drinkwatervoorziening, is overleden.
1935;3;11;De eerste lichtsignalen tot regeling van het verkeer aan de Leidscheweg bij de Mineurslaan zijn in werking gesteld.
1935;3;12;Tot de 21e van deze maand is de Tweeendertigste Nederlandse Jaarbeurs.
1935;3;17;W.K.D. Lustig, oud-lid van de Gemeenteraad en lid van de Provinciale Staten, is overleden.
1935;3;21;De arts Dr. J.G. van der Kaay is overleden.
1935;3;23;Mr. N.J.C.M Kappeyne van de Coppelo is in Loenen benoemd tot lid van de Provinciale Staten in de vacature ontstaan door de benoeming van Jhr. Ir. O.C.A. van Lidth de Jeude tot Minister van Waterstaat.
1935;3;29;H.B. Berghuys is benoemd tot lid van de Provinciale Staten in de vacature ontstaan door het overlijden van W.K.D. Lustig.
1935;3;31;Dr. A.H. van der Hoeve, inspecteur bij het Gemeentelijk lager onderwijs, verlaat de gemeentedienst.
1935;3;31;Dr. Ir. G.W. Heukelom, hoofd-ingenieur van de Nederlandse Spoorwegen, verlaat de dienst.
1935;4;6;J. Bochove is in Zeist benoemd tot lid van de Provinciale Staten in de vacature ontstaan door het overlijden van A. Hoogendijk.
1935;4;8;Mr. R.A. Vogelsang, oud-secretaris van de Utrechtse Kamer van Koophandel en fabrieken, is overleden.
1935;4;10;Jhr. Mr. D. Rutgers van Rozenburg is geinstalleerd als substituur-griffier bij de Rechtbank.
1935;4;23;Verkiezing van Leden van de Provinciale Staten. Gekozen 11 Rooms-Katholieken, 9 leden van de SDAP, 7 Anti-Rev, 5 Christ. Hist, 4 leden van de NSB, 2 leden van de Staatskundig Gereformeerde Partij, 2 leden van de Vrijheidsbond en 1 Vrijzinnige Democraat.
1935;4;30;Dr. F.H. van der Laan, directeur van de Keuringsdienst voor waren voor het gebied Utrecht, verlaat de gemeentedienst.
1935;5;6;Dr. S.F.H.J.J. Berkelbach van den Sprenkel, hoogleraar aan de Rijksuniversiteit, houdt zijn inaugureele rede/
1935;5;18;H.K.M. Prinses Juliana opent de Rode Kruisdag.
1935;5;18;Directeur van de NV A.W. Bruna en Zoon's Uitgevers-maatschappij, G. Bruna W. Az, is overleden.
1935;5;21;J.A. van Straaten, directeur van de NV Eerste Nederlandse Asphaltfabriek voorheen Gurtzen en Van Straaten, is overleden.
1935;5;21;De Chinese kok, Lee Foo, van restaurant Chung Ho Cha Lou aan de Lichtegaard heeft zijn collega, Kiou Gim Fan, vermoord en daarna zelfmoord gepleegd door van het dak te springen.
1935;6;1;Dr. G.W. Kernkamp, hoogleraar aan de Rijksuniversiteit, houdt zijn afscheidscollege.
1935;6;1;Obbe Norbuis wordt geïnstalleerd als burgemeester van Zuilen. Hij volgt mr. M.M. Kwint op die als tijdelijk burgemeester benoemd was voor Zuilen.
1935;6;3;De opgravingen op het Domplein worden voortgezet.
1935;6;19;C.J.H. Muller, directeur van de NV. Weduwe P. Smits en Zonen, is overleden.
1935;6;22;De componiste Hendrika van Tussenbroek is in Doorn overleden.
1935;6;25;Verkiezing van leden van de Gemeenteraad. Uitgebracht 71710 geldige stemmen. Gekzoen 14 leden van de SDAP, 12 Rooms-Katholieken, 5 AR, 3 Chr. Hist, 2 leden van de Vrijheidsbond, 1 Vrijzinnige Democraat, 1 lid van de Communistische Partij en 1 van de groep Gemeentebelangen.
1935;6;27;Jhr. Mr. W.E. Bosch van Oud-Amelisweerd, oud-notaris en oud-lid van de Provinciale Staten, is overleden.
1935;7;5;Dr. D. Plooy, hoogleraar aan de Rijksuniversiteit, is overleden.
1935;7;15;De Feestweek georganiseerd door de Vereniging voor Vreemdelingenverkeer "Utrecht", begint.
1935;7;24;Dr. F.A.F.C. Went, oud-hoogleraar aan de Rijksuniversiteit, is in Wassenaar overleden.
1935;7;31;De daktuin op het hotel "Pomona" is officieel geopend.
1935;8;7;Dr. J. de Jong, kanunnik van het Metropolitaan Kapittel en president van het Groot-Seminarie in Rijsenburg, is benoemd tot coadjutor, met het recht van opvolgen van de aartsbisschop, Mgr. J.H.G. Jansen.
1935;8;8;De Gemeenteraad besluit tot de bouw van de brug over de Weerdsingel in de verkeersweg Vredenburg-Pijlsweerd.
1935;8;29;De Gemeenteraad besluit tot het aangaan van overeenkomsten met het Rijk en de Staatsspoorwegen ter uitvoering van de plannen tot verbetering van de Spoorweg- en Verkeerstoestanden.
1935;8;29;Afscheid van A.H. Smulders als wethouder en van G. Serton als raadslid.
1935;9;2;De verbeterde zittingszaal van de rechtbank is in het gerechtsgebouw aan de Hamburgerstraat in gebruik genomen.
1935;9;2;De Roomskatholieke kerk der parochie van de Heilige Gerardus Majella aan de Thomas a Kempisweg-hoek Vleutenseweg, is ingewijd.
1935;9;2;Tot de 12e van deze maand is de drieendertigste Nederlandse Jaarbeurs.
1935;9;3;De Gemeenteraad besluit het aantal wethouders op 5 te brengen. Gekozen worden G.L. de Haas, J.J. Reynders, H. Botterweg, H.A. Bekker en mr. J.H. van Maarseveen.
1935;9;11;H.M. de Koningin bezoekt de vee-tentoonstelling.
1935;9;12;Dr. J. de JOng, coadjutor van de aartsbisschop, is tot bisschop gewijd.
1935;9;16;Dr. H. Bolkestein draagt het rectoraat van de Rijksuniversiteit over aan Dr. C.W. Vollgraff.
1935;9;27;Mej. G.C. Horsman promoveert in de Spaanse taal tot doctor in de letteren en wijsbegeerte.
1935;9;28;Ir. J.J. L. Smits, directeur van het gas- electriciteits- en trambedrijf, neemt als zodanig afscheid.
1935;10;7;Dr. J.F.A.M. Buffart. hoogleraar aan de Rijksuniversiteit, houdt zijn inaugureele rede.
1935;10;10;W.C. Vuystingh, oud-notaris, is overleden.
1935;10;12;De geschiedkundige Jhr. Ir. G.G. Calckoen is overleden.
1935;10;25;H.K.H. Prinses Juliana opent het nieuwe laboratorium voor Physiologische Chemie aan de Vondellaan en brengt een bezoek aan een paar univeritaire inrichtingen.
1935;10;26;Bespeling van het Dom-Carillon ter gelegenheid van het door de besturen van het NVV en de SDAP bijeengeroepen congres voor het Plan van Arbeid.
1935;10;27;De godsdienstonderwijzer P. Meyers, leidt voor de laatste keer een dienst in de Marnixzaal van de Kinderkerk.
1935;10;31;P.J.H. van de Weyer, emeritus-pastoor van de Rooms Katholieke Parochie van St. Willibrordus aan de Minrebroederstraat, is overleden.
1935;11;5;Dr. G. Gosses, hoogleraar aan de Rijksuniversiteit, houdt zijn inaugureele rede.
1935;11;6;De drukker en uitgever P. den Boer is overleden.
1935;11;7;Het laboratorium voor Vergelijkende Physiologie aan de Alexander Numankade, is officieel geopend.
1935;11;20;J. Beumer, oud-godsdienstonderwijzer van de Nederlands Hervormde Gemeente, is overleden.
1935;11;23;De Utrechtse Christelijke Jongemannenvereniging bestaat 60 jaar.
1935;11;23;De nieuwe districts-indeling van het automatische telefoonnet is in gebruik gesteld.
1935;11;25;Dr. H.L. Grondijs, hoogleraar aan de Rijksuniversiteit, houdt zijn inaugureele rede.
1935;11;27;J.J. Vinkesteijn, oud-hoofdredacteur van de Utrechtse courant, is overleden.
1935;11;28;Het nieuwe Centraal Militair Hospitaal is aanbesteed.
1935;11;29;Ds. J.A. Rust viert zijn 40-jarig ambtsjubileum als predikant van de Hersteld-Evangelisch-Lutherse kerk.
1935;12;2;Dr M.J.A. De Vrijer, hoogleraar aan de Rijks-Universiteit, houdt zijn inaugureele rede.
1935;12;17;De provinciale staten besluiten 5000 gulden beschikbaar te stellen ter gelegenheid van de viering van het derde eeuwfeest van de Rijks-Universiteit.
1935;12;20;De laatste van de drie Utrechtse naaischolen in de Mgr. Van de Weteringstraat, is op advies van het bestuur van de Johanna-Stichting gesloten.
1935;12;24;A. van Eijsden Jr., oud-directeur van de voormalige Utrechtse Tram-maatschappij (paardentram) is overleden.
