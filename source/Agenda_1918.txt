1918;1;1;Vandaag 85 jaar geleden, trad A. v.d. Brink in dienst als machinist op het stoomgemaal Hoog en Laag Raven te Jutphaas.
1918;1;2;Deze woensdag is het 25 jaar geleden dat D. Methorst in dienst kwam bij de firma Duyvis.
1918;5;27;Het is vandaag precies 100 jaar geleden dat F.C. Donders werd geboren. Zijn beeld op het Janskerkhof zou vandaag onthult worden. Helaas heeft dat wegens de Eerste Wereldoorlog vertraging opgelopen.
1918;12;16;De weg langs de Vecht, tussen de Roode Brug en de fabriek van de NV Machinefabriek Hoogenlanden, vh Pannevis en Zn, is tot nader bericht afgesloten voor al het rijdend verkeer.
1918;12;16;Bij de in Utrecht gehouden vergadering van de Marconistenbond wordt besloten de naam te wijzigen in "Nederlandse Vereeniging van Radio-telegrafisten ter koopvaardij".
1918;12;16;Bij een inval in een perceel aan de Varkensmarkt wordt achter de wand van een leeg konijnenhok een clandestiene slachterij ontdekt.
1918;12;20;In de Utrechtse Schouwbug wordt door de koninklijke Nederlandse vereniging Het Nederlands Toneel het blijspel van Lothar Schmidt, "Maar 'n Droom" opgevoerd.
