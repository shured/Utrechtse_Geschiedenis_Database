1988;1;1;In 1987 is het inwonertal van de provincie Utrecht toegenomen met 9.423 tot 965.148. Het inwonertal van de stad Utrecht is voor het eerst sinds 1980 weer gegroeid en wel met 213 tot 229.498 personen.
1988;1;1;De gemeentearchivaris van Utrecht, dr. J.E.A.L. Struick treedt vervroegd uit. Hij wordt opgevolgd door drs. I.W.L.A. Caminada.
1988;1;3;Voor het cultureel centrum de Speeldoos in Baarn wordt het door de Amsterdamse beeldhouwer Arie Teeuwisse vervaardigde beeld Arlechino onthuld.
1988;1;6;Ing. P.A.D.H. Diepenhorst wordt als burgemeester van Maartensdijk geïnstalleerd.
1988;1;11;Wethouder drs. W.P.C. van Willigenburg (VVD) van Openbare Werken opent in het Gemeentelijk Informatie Centrum Utrecht de tentoonstelling "De gemeente Utrecht en riolering".
1988;1;12;Met het opheffen van de veenderijfondsen en de verdeling van de gelden aan de gronteigenaren komt er een eind aan vier eeuwen gereglementeerde vervening in de provincie.
1988;1;13;Mr. R.B.M. Berger wordt geïnstalleerd als hoofdofficier van justitie in Utrecht als opvolger van de naar Rotterdam vertrokken mr A. Herstel.
1988;1;13;In de Pieterskerk in Utrecht wordt de gedichtenbundel "Utrecht in Wolken en skeletten" van Charles van Leeuwen met tekeningen van Tom Eyzenbach gepresenteerd.
1988;1;18;Tot inspraak-coördinatrice van de provincie Utrecht is genoemd H. Witbraad-Wiltink.
1988;1;18;Ter gelegenheid van het 25-jarig bestaan van het gereformeerde bejaardentehuis "De Lichtkring" verschijnt het door A.H.J. Horjes geschreven boekje "Leve de Lichtkring".
1988;1;20;In het in 1921 door architect J. Crouwel ontworpen voormalige anatomische laboratorium van de diergeneeskundige faculteit van de Rijksuniversiteit Utrecht aan de Bekkerstraat wordt het Provinciaal Jeugdtheater Platform geopend met de première van "Uilenspiegel".
1988;1;22;De gereformeerde wijkgemeente Zuid/Oost neemt het nieuwe kerkcentrum aan de Gansstraat in gebruik. Dit centrum is gebouwd op de plaats van de afgebroken Zuiderkerk.
1988;1;22;Ter gelegenheid van het 50-jarig bestaan van de Stichting voor Esthetische Vormgeving Artibus wordt in de Crediet- en Effectenbank aan de Kromme Nieuwegracht in Utrecht een tentoonstelling van werk van leerlingen geopend.
1988;1;25;De Boerderijenstichting Utrecht, die zich inzet voor het behoud van de karakteristieke verschijningsvormen van de Utrechtse boerderijen en hun erven, wordt opgericht.
1988;1;27;In het gemeentehuis van Breukelen presenteert de pas ingestelde Begeleidingscommissie Ondersteuning Musea zich aan de vertegenwoordigers van de musea en de historische verenigingen in de provincie Utrecht.
1988;1;28;In het hoofdgebouw van politie aan de Kroonstraat in Utrecht opent wethouder Van Willigenburg (VVD) een fototentoonstelling ter gelegenheid van het eenjarig bestaan van de milieupolitie.
1988;1;31;De Utrechtse beeldhouwer Pieter d'Hont reikt voor de eerste maal de naam hem vernoemde, op 24 april 1987 ingestelde, prijs uit aan de Nijkerkse beeldhouwer Wouter Bouwman.
1988;2;2;De collectie van het Nederlands Spoorwegmuseum wordt uitgebreid met een seinhuis van de Staatsspoorwegen uit 1900. Het is afkomstig uit Elst in Gelderland.
1988;2;3;Op 79-jarige leeftijd overlijdt in Tull en 't Waal de in Utrecht geboren surrealistische schilder J.H. Moesman.
1988;2;3;Staatssecretaris drs. E. Heerma van Volshuisvesting geeft samen met wethouder M.M. Andriessen-Brockman het officiele startsein voor de bouw van 132 huurwoningen in de sociale woningbouw op het Hamburgerterrein aan de Leidsche Rijn in Utrecht.
1988;2;4;Wethouder C. Pot van Onderwijs houdt in de Stadsschouwburg in Utrecht de gemeentelijke instelling voor basiseducatie ten doop, die het onderwijs aan volwassenen die onvoldoende scholing hebben gehad, moet bundelen.
1988;2;6;Minister drs. J. de Koning van Sociale Zaken opent een onderwijsproject over relaties en sexualiteit, getiteld "Samen Leven", speciaal gericht op leerlingen uit het voortgezet onderwijs.
1988;2;7;Wethouder F. Asselbergs van Ruimtelijke Ordening opent in de Zonnehof in Amersfoort de expositie "Een veranderd Stadsbeeld", over Amersfoort vroeger, nu en in de toekomst.
1988;2;11;De Stichting Huisorgel Collectie wordt opgericht. De stichting zet zich er voor in om in de Pieterskerk in Utrecht een groot aantal huisorgels te verzamelen.
1988;2;12;In het Centraal Museum in Utrecht wordt de tentoonstelling "7 maal 4 x 4" geopend van panelen gemaakt door Utrechtse kunstenaars voor het Shaffytheater in Amsterdam.
1988;2;12;Milieugedeputeerde ir. G. van Wijnbergen heropent de in 1979 gesloten vuilstortplaats in Maarsbergen, nu voor de winning van methaangas.
1988;2;15;Gedeputeerde Van Wijnbergen start in Leusden een proefproject voor de inzameling van chemisch afval dat overblijft bij bouwwerkzaamheden.
1988;2;16;Met de derde Nicolaas van der Mondelezing die prof. dr. ir. C.K. Temminck Groll in een volle Pieterskerk houdt over de Romaanse kerken van Utrecht, gaat de manifestatie "Utrecht, Middeleeuwse kerkenstad" officieel van start.
1988;2;21;De in 1974 aan de noordkant van het Vredenburg door de gemeente Utrecht aangebracht luifels worden wegens hun slechte staat weggehaald.
1988;2;24;Tijmen Kater, die 25 jaar secretaris was bij de Koninklijke Nederlandse Natuurkundige Vereniging, ontvangt bij zijn aftreden uit handen van wethouder Van Willigenburg (VVD) de Maartenspenning van de stad Utrecht.
1988;2;25;Wethouder Pot start het spijbelopvangproject "Tijdelijke Opvang Voortijdige Schoolverlaters" aan de Voortrekkersdreef in Utrecht.
1988;2;25;In park De Vechtzoom in Utrecht wordt een beeld van de Maarssense kunstenares Elselien de Graaf geplaatst, voorstellende drie naar de jeu de boulesspel kijken figuren.
1988;2;26;Loco-burgemeester Pot van Utrecht installeert Hans Kroon als raadslid voor de PPR als opvolger van de onlangs afgetreden Mar Dekker.
1988;2;26;Ter gelegenheid van het 10-jarige bestaan van de biologiewinkel van de Rijksuniversiteit verschijnt het door deze instelling en de werkgroep Herstel Leefbaarheid Oude Stadswijken samengestelde "Utrechts Singelboek".
1988;2;29;Met de ondertekening van de samenwerkingsovereenkomst tussen de electriciteitsbedrijven van Noord-Holland, Amsterdam en Utrecht wordt het nieuwe energieproductiebedrijf UNA officeel een feit.
1988;3;1;Er wordt begonnen met de verbouw van de Sint Martinuskerk aan de Oudegracht in Utrecht waarin 37 appartementen opgenomen zullen worden.
1988;3;2;Het, naar het voorbeeld van 1 van de paleizen in de verboden stad in Peking gebouwde, hotel Oriental Palace in Breukelen wordt officieel geopend.
1988;3;4;In het Rijksmuseum Het Catharijneconvent in Utrecht wordt de tentoonstelling "Helse en Hemelse vrouwen" geopend.
1988;3;4;Op 80-jarige leeftijd overlijdt A.F. Wolff, de oprichter van het bedrijf dat in Utrecht sinds 1939 bijna alle bioscopen beheert.
1988;3;11;Bekend wordt dat in het Willem Arntsz Huis aan de Lange Nieuwstraat in Utrecht het nieuw op te richten Museum Geestelijke Gezondheidszorg zal worden gevestigd.
1988;3;12;Het voormalige Gebouw voor Kunsten en Wetenschappen aan de Mariaplaats, waarin een gedeelte van het Utrechts Conservatorium is ondergebracht, wordt grotendeels door brand verwoest. Daarbij gaat ook een aantal zeer kostbare muziekinstrumenten verloren. De brand begint op vrijdag de 11e om kwart over elf 's avonds.
1988;3;14;Na 132 jaar elders te hebben gestaan, wordt de vishal van Amersfoort weer op zijn oorspronkelijke plaats op de Kortegracht geplaatst.
1988;3;16;Drs. J.J.F.M. Westra wordt geïnstalleerd als burgemeester van Vleuten-de Meern als opvolger van H.A.C. Middelweerd, die met pensioen ging.
1988;3;19;In het gebouw Artishock, het voormalige klooster van Sint Jozef in Soest, wordt een oudheidkundige museum geopend.
1988;3;21;Milieuwgedeputeerde Van Wijnbergen en de Utrechse wethouder Van Willigenburg (VVD) maken een begin voor de aanleg van een grenenhouten geluidswal langs de rijksweg 722 en de A12 vanaf de Tolsteeg tot het Merwedekanaal.
1988;3;23;Mevrouw M.E.J. Lubbers-Hoogeweegen, de echtenote van de minister-president Ruud Lubbers, opent het nieuwe hoofdkantoor van de Stichting Kinderopvang Nederland aan de Europalaan.
1988;3;25;Ter gelegenheid van de 325e Dies Natalis van de Rijksuniversiteit Utrecht worden eredoctoraten verleend aan de schrijfster Hella Haasse en de Natuurkundige prof. dr. I. Sick.
1988;3;25;Drs. M.W.M. Vos-van Gortel, burgemeester van Utrecht, opent het nieuwe gebouw van de RIAGG aan de Tolsteegsingel.
1988;3;29;Een groep van 45 jongeren blokkeert de hoofdingang van het Utrechtse stadhuis om daarmee te protesteren tegen de plannen van de regering om de sociale uitkeringen en studiebeurzen te verlagen.
1988;4;6;Beked wordt dat de op 1 na oudste amateurvereniging van Utrecht, de Rederijkamer Nicolaas Beets, na 78 jaar wegens teruglopend ledental is opgeheven.
1988;4;8;Burgemeester Vos opent het aan het Westplein in Utrecht gebouwde hotel Scandic Crown.
1988;4;9;In aanwezigheid van de ambassadeurs van diverse Afrikaanse landen opent wethouder F.H. van der Sluijs het Afrikaans Cultureel Centrum aan de Plompetorengracht.
1988;4;12;Ter gelegenheid van het feit dat het ontwerp voor het Wilhelminapark in Utrecht 100 jaar geleden door Henri Copijn werd gepresenteerd, wordt in het park een Copijnboom geplant en wordt het boek "Parken in Utrecht" gepresenteerd.
1988;4;14;het voormalig lid van Gedeputeerde Staten ir. J.D. Hoepelman wordt directeur van de Pegus, als opvolger van ir. F. de Ruiter.
1988;4;14;Bij zijn afscheid als voorzitter van de Kring van Utrechtse Ondernemingen ontvangt H.R. Bredero de zilveren stadsmedaille.
1988;4;15;Burgemeester Vos opent in de Crediet- en Effectenbank aan de Kromme Nieuwegracht in Utrecht de tentoonstelling "Oude kerken, nieuwe luister".
1988;4;15;Op het Hobbemaplein, bij de Jan van Scorelstraat, in Utrecht wordt het door de Amsterdamse beeldhouwer Herman Janzen vervaardigde beeld van de schilder Jan van Scorel geplaatst.
1988;4;16;Gedeputeerde J. Hoekstra van Cultuur opent de geheel gerestaureerde molen De Windotter in IJsselstein.
1988;4;20;Bij uitgeverij Matrijs verschijnt het boekje "De Plek", waarin 21 Utrechters vertellen over hun meest dierbare plekje in de stad.
1988;4;21;Drs. B. Wallet, predikant van de Mattheuskerk in Oog in Al in Utrecht, wordt gekozen tot voorzitter van de Hervormde Synode.
1988;4;21;De Utrechtse gemeenteraad gaat akkoord met het voorstel van burgemeester en wethouders om 200.000 gulden beschikbaar te stellen voor de financiering van het project "Wonen boven winkels" teneinde het wonen in de binnenstad te stimuleren.
1988;4;22;Met de opening van de tentoonstelling "Het Agnietenklooster (nu het Centraal Museum) te Utrecht" begint het Centraal Museum de festiviteiten rond zijn 150-jarig bestaan.
1988;4;23;In het Stadsmuseum Woerden worden expositie van werk van hedendaagse Woerdense kunstenaars en de overzichtstentoonstelling van etsen van Hermanus van Swanevelt geopend.
1988;4;23;Ter gelegenheid van de landelijke bibliotheekdag verschijnen er twee literaire wandelgidsen voor de Utrechtse binnenstad, 1 voor kinderen en 1 voor volwassenen.
1988;4;27;Minister drs. E.H.T.M. Nijpels van Volkshuisvesting, Ruimtelijke Ordening en Milieubeheer stelt het geautomatiseerde vastgoedsysteem van het Utrechtse kadaster aan het Herculesplein officieel in werking.
1988;4;30;In de Oudheidskamer Vreeswijk wordt de tentoonstelling geopend over de bouw, 50 jaar geleden, van de Beatrixsluis.
1988;5;2;Vijf maanden later dan gepland wordt, na vele tegenslagen, door wethouder drs. R.M. ten Cate-Dhont van Sociale Zaken een 30 meter hoge windmolen op het terrein van de GEB aan de Nijverheidsweg in Utrecht in gebruikt gesteld.
1988;5;3;De AMEV-prijzen voor milieu worden door wethouder Van Willigenburg (VVD) uitgereikt aan de Stichting Vogelopvang Utrecht, de Bijenhouders-vereniging Utrecht en het Milieu Informatie Centrum Utrecht.
1988;5;3;In het Provinciehuis aan het Achter Sint Pieter in Utrecht worden de Stichting Publicaties Oud-Utrecht en haar eerste uitgave de Provinciale Archeologische Kroniek gepresenteerd.
1988;5;5;Bij Slot Zeist worden aan gedeputeerde J. Hoekstra het door Saskia Bongaerts samengestelde boekje met zes fietsroutes langs musea in de provincie en 1 wandelroute langs de musea in de stad Utrecht aangeboden.
1988;5;7;Ter gelegenheid van haar eerste lustrum opent de afdeling Utrecht van het Instituut voor Natuur- en Milieu-educatie een natuurpad in de wijk Overvecht.
1988;5;7;Met een rolstoelmarathon van Leersum naar Utrecht wordt een begin gemaakt met de verhuizing van het revalidatiecentrum De Hoogstraat van Leersum naar de nieuwe behuizing in Utrecht, die deze maand een feit moet worden.
1988;5;8;Aan het Rotsoord wordt het terrein van Vogelopvang Utrecht officieel in gebruik genomen.
1988;5;9;Met het onthullen van een plaquette opent burgemeester Vos de nieuwbouw van het project Kruisstraat in Utrecht.
1988;5;9;In Amsterdam overlijdt op 74-jarige leeftijd de musicus Johan van den Boggert. Hij was van 1958 tot 1979 directeur van het Utrechts Conservatorium.
1988;5;9;Ter gelegenheid van het 300-jarig bestaan van de Amersfoortse Lutherse Gemeenschap verschijnt een door Henk Poots geschreven herdenkingsboek.
1988;5;11;De Utrechtse seriemoordernaar Hans van Zon is overleden in Delfzijl. Als doodsoorzaak wordt zijn overmatig en langdurig drankmisbruik genoemd.
1988;5;14;In de Domkerk in Utrecht wordt een tentoonstelling geopend van aquarellen die de Zwolse schilder Johan Grabijn maakte van de Utrechtse kerken vanuit dezelfde positie als vanwaaruit Pieter Saenredam deze schilderde.
1988;5;15;Ter gelegenheid van het 40-jarig bestaan van Dierenpark Amersfoort wordt in het park de tentoonstelling "Futari" geopend met onder andere dierplastieken van Kees Ouwens en Jits Bakker.
1988;5;15;In de Nicolaikerk in Utrecht wordt de tentoonstelling "De Nicolaikerk, een monument voor vele gebruikers" geopend.
1988;5;19;Met de ingebruikneming van de nieuwe loketten wordt de eerste fase van de herinrichting van de hal van het Centraal Station in Utrecht afgesloten.
1988;5;20;In het Dioscesaan Pastoraal Centrum Dijnselburg in Zeist wordt door kardinaal Simonis een bronzen borstbeeld van de vorig jaar overleden kardinaal Alfrink onthuld.
1988;5;20;Op 73-jarige leeftijd overlijdt dr. J. Spaander. Hij was van 1950 tot 1979 directeur van het Rijksinstituut voor de Volksgezondheid en Milieu-hygiene.
1988;5;25;Wethouder Pot onthult op het Utrechts Buitencentrum in Oldebroek het ter gelegenheid van het 25-jarig bestaan door Marijke van Os vervaardigde kunstwerk, getiteld Schuitje varen, theetje drinken.
1988;5;26;Als waarschuwingsaktie tegen de voorgestelde halvering van het aantal arbeidsplaatsen gaat het personeel van het Bureau van de Utrechtse Universiteit in staking.
1988;5;26;Bij de viering van het 60-jarig bestaan van de Utrechtse Klokkenspel Vereniging ontvangt burgemeester Vos de delen 2 en 3 van de Utrechtse Beiaardboeken.
1988;5;27;In de binnentuin van het Bartholomeus Gasthuis in Utrecht wordt een door de ondernemersvereniging Lange Smeestraat aangeboden Zonnewijzer overgedragen aan de bewonerscommissie van het onlangs verbouwde bejaardentehuis.
1988;5;27;Bij zijn afscheid als geneesheer-directeur van het centrum voor reactivering en verpleging Albert van Koningsbruggen ontvangt dr. J.A. Stoop de Speld van de stad Utrecht.
1988;5;27;Ir. T. van Hoogevest ontvangt de Archonormprijs 1988 uit handen van de vorige winnaar, Mart van Schijndel, voor het werk dat zijn bureau heeft verricht voor de restauratie van de vijf hervormde Utrechtse binnenstadskerken.
1988;6;2;Minister van Justitie mr. F. Korthals Altes opent de beurs "Informatie door Communicatie" die is georganiseerd door de Politie Verbindings Dienst.
1988;6;2;Het is 75 jaar geleden dat de Holy Trinity Church aan de Van Limburg Stirumstraat in Utrecht werd ingewijd.
1988;6;9;Ir. T. van Hoogevest ontvangt het eerste exemplaar van een door Jo van Staveren geschreven boekje, getiteld "De Dom" met een beschrijving van de Domkerk in Utrecht.
1988;6;9;In de Jacobikerk in Utrecht wordt de tentoonstelling "Kluizenaressen in Utrecht" geopend.
1988;6;11;Statenlid G. Geitenbeek opent op het industrieterrein Lageweide een project dat is opgezet voor twaalf autosloperijen die vroeger op het woonwagenkamp De Huppel zaten.
1988;6;16;De Utrechtse gemeenteraad besluit een ombudsman aan te stellen om klachten van burgers over het functioneren van het gemeentelijk apparaat te behandelen.
1988;6;16;De vertrekkende gemeentesecretaris van Utrecht drs. C.J.N. Versteden wordt opgevolg door de uit Nieuwerkerk aan de IJssel komende drs. A. Vermeulen.
1988;6;16;Geduputeerde Hoekstra en burgemeester de Graaf van Leersum stellen een fiets/wandelroute langs de ongeveer 50 grafheuvels van de Utrechtse Heuvelrug in gebruik.
1988;6;16;Door het luiden van een scheepsbel stelt de commissaris van de koningin Beelaerts van Blokand, de nieuwe drukpers van drukkerij De Boer-Cuperus BV in werking. Tevens overhandig hij aan jhr. C.J. Boreel, directeur van de drukkerij, het Certificaat Produktbewaking.
1988;6;17;In aanwezigheid van koningin Beatrix wordt de laatste van de vijf gerestaureerde middeleeuwse kerken in Utrecht, de Domkerk, overgedragen aan de Hervormde Gemeente. Dit is ook het startsein voor de manifestatie "Utrecht, Middeleeuwse Kerkenstad" die tot november 1988 zal duren.
1988;6;17;De faculteit der diergeneeskunde van de Rijksuniversiteit Utrecht neemt met een Bourgondische maaltijd afscheid van het terrein aan de Biltstraat nadat ook de laatste afdeling is verhuisd naar de Uithof.
1988;6;20;Burgemeester Vos en vertegenwoordigers van het Algemeen Burgerlijk Pensioenfonds, de Koninklijke Jaarbeurs en de Nederlandse Spoorwegen, ondertekenen het "Utrecht City Project" waarin een ingrijpende facelift van Hoog Catharijne en omgeving wordt voorgesteld.
1988;6;24;In het kader van de bezuinigingsoperatie "Taakverdeling en Concentratie" van het ministerie van Onderwijs en Wetenschappen, sluit het meer dan 100 jaar oude Tandheelkundig Instituut van de Rijksuniversiteit Utrecht.
1988;6;24;Wethouder Van der Sluijs en het kamerlid Erica Terpstra openen in Utrecht de drukkerij DomCom (DOven COMmunicatie) en de Nederlandse Dovenwinkel aan de Trans.
1988;6;24;Utrecht en De Bilt krijgen met acht andere gemeentes van de Stichting Landelijke Organisatie Belangengroepen Huisvesting en de Stichting de Ombudsman een aanmoedigingsprijs voor hun beleid op het gebied van de verdeling van woonruimte.
1988;6;25;Ter gelegenheid van haar 50-jarige bestaan presenteert de Utrechtse Wandelsportvereniging Op Sterke Benen een boekje met 20 wandeltochten in en om Utrecht.
1988;6;20;Minister Brinkman van Welzijn, Volksgezondheid en Cultuur opent de nieuwbouw van de Rijksdient voor Oudheidkundig Bodemonderzoek aan de Muurhuizen in Amersfoort. Het ontwerp is van de Amsterdamse architect Abel Cahen.
1988;6;30;De chef culturele zaken van de gemeente Utrecht, drs. B.J. Peiser, treedt vervroegd uit. Hij wordt opgevolgd door mr. H.B.C. van Tilburg.
1988;7;5;In Museum Flehite in Amersfoort wordt de tentoonstelling "WATER als vriend en vijand in het Oostelijke deel van de provincie Utrecht" geopend.
1988;7;5;Burgemeester Schreuder van Amersfoort neemt het eerste exemplaar van het door W.J. Beks geschreven boekje over het 80-jarig Amersfoorts mannenkoor in ontvangst.
1988;7;5;In het kader van de William en Mary herdenking wordt in Slot Zeist de tentoonstelling "Bouwen en wonen in het gevolg van de Koning-Stadhouder" geopend.
1988;7;8;In het Rijksmuseum Het Catharijneconvent in Utrecht wordt de tentoonstelling "Utrecht, een hemel op aarde" geopend.
1988;7;11;Op 76-jarige leeftijd overlijdt in Zeist dr. H. Schamhardt. Hij was 15 jaar lang in dienst van de Rijksuniversiteit Utrecht. Eerst als secretaris van het College van Curatoren en later als lid van het College van Bestuur.
1988;7;12;In Slot Zuylen wordt de tentoonstelling "Spelen op Zuylen" geopend, die ingericht is door de Vereniging van Vrienden van Slot Zuylen met speelgoed van de vroegere bewoners, de familie Van Tuyll van Serooskerken.
1988;7;13;Op 51-jarige leeftijd overlijdt in Utrecht Huub Bals, de organisator van de Cinemafestaties in Utrecht en directeur van filmhuis 't Hoogt. Hij werd later oprichter en artistiek directeur van eht Rotterdams Filmfestival.
1988;7;15;Burgemeester Vos opent in het Centraal Museum de tentoonstelling "Utrecht op schilderijen".
1988;7;20;In het kader van Utrecht Middeleeuwse Kerkenstad wordt het Maliebaan-festival gehouden met een kermis, muziekgroepen, clowns, acrobaten en een eucharistieviering op de autoscooterbaan.
1988;7;26;In de Crediet- en Effectenbank aan de Kromme Nieuwegracht in Utrecht wordt tentoonstelling "Op het spoor van bussen" geopend, over de Algemene Transport Onderneming, die in 1927 als dochter van de Nederlandse Spoorwegen werd opgericht.
1988;7;29;Het Van der Valk-concern koopt het hotel Oriental Palace in Breukelen dat op 2 maart 1988 werd geopend en op 8 juni alweer failliet werd verklaard.
1988;8;1;Tot bijzonder hoogleraar klinische sportgeneeskunde bij het vorige jaar opgerichte Janus Jongbloed Research Centrum van de Rijksuniversiteit Utrecht, is benoemd de Amersfoortse cardioloog dr. W.L. Mosterd.
1988;8;6;In Warmond overlijdt op 71-jarige leeftijd H.G.I. baron van Tuyll van Serooskerken. Hij was van 1970 tot 1974 burgemeester van Utrecht.
1988;8;15;Het Nederlands Spoorwegmuseum te Utrecht sluit de binnenexpositie voor ongeveer een jaar in verband met verbouwing en reorganisatie.
1988;8;18;Bekend wordt dat op industrieterrein De drie Eiken te Baarn archeologische vondsten zijn gedaan uit de late en midden steentijd.
1988;8;24;Burgemeester Vos ontvangt het eerste exemplaar van het door Thea Beckman geschreven jeugdboek De val van de Vredeborch.
1988;8;25;Ds. C. W. de Planque neemt afscheid als algemeen directeur van het Diakonessenhuis te Utrecht, hij wordt opgevolgd door drs. P. A. Gille.
1988;8;25;In de Pieterskerk wordt het Middeleeuwse mirakelspel 'het Spel van de Heilige Sacrament van der Nieuwervaert' opgevoerd, in het kader van de manifestatie 'Utrecht Middeleeuwse Kerkenstad'.
1988;8;31;De commandant van de Rijkspolitie in het district Utrecht, W. D. Lanting, verlaat de dienst wegens het bereiken van de pensioengerechtigde leeftijd.
1988;8;31;Ter gelegenheid van zijn 75-jarig bestaan schenkt het Amersfoortse Waterbedrijf aan de bewoners van de stad een fontein in de gracht aan de Westsingel bij het museum Flehite.
1988;9;1;Dr. W. J. Hardeman wordt benoemd tot algemeen directeur van het Willem Arntsz Huis.
1988;9;1;Bij het district Utrecht van de rijkspolitie treedt als commandant in dienst kolonel M. P. Baars. Zij is de eerste vrouwelijke districtscommandant in Nederland.
1988;9;2;Wethouder Pot opent de tentoonstelling 'De Ideale Buurt' over het verleden en de toekomst van de Breedstraatbuurt te Utrecht.
1988;9;4;In het Museum Hedendaagse Kunst Utrecht wordt de tentoonstelling '50 jaar Artibus' geopend met werk van leerlingen en oud-leerlingen.
1988;9;7;Drs. J. W. Janssen, o.a. oud-voorzitter van het Utrechts Landschap, ontvangt uit handen van de commissaris van de koningin jhr. Beelaerts van Blokland de provinciale legpenning.
1988;9;8;In het gebouw van de Utrechtse Gemeentelijke Archiefdienst wordt een tentoonstelling geopend van 24 aquarellen die Anthony Grolman in 1889 maakte van zalen van het Museum van Oudheden (nu Centraal Museum) op de verdieping van het stadhuis.
1988;9;9;Op 85-jarige leeftijd overlijdt ir. F. Vos, oud-directeur van houthandel Jongeneel te Utrecht. Hij was o.a. de eerste voorzitter van het Koningin Wilhelmina Fonds.
1988;9;9;Prinses Margriet opent de tentoonstelling ter gelegenheid van het 150-jarig bestaan van het Centraal Museum, getiteld 'De Ideale Stad, ideaalplannen voor de stad Utrecht, 1664-1988'.
1988;9;12;Op 72-jarige leeftijd overlijdt te Utrecht Stoffel van Viegen. Hij was bijna 50 jaar organist van de Domkerk.
1988;9;15;Burgemeester Vos ontvangt het eerste exemplaar van de bundel Op staal gefundeerd, door het Historisch Onderzoeksburo Histodata samengesteld, ter gelegenheid van het 75-jarig bestaan van de van oorsprong Zuilense woningbouwvereniging K 77.
1988;9;16;Burgemeester Vos en wethouder Van Willigenburg (VVD) stellen de zogenaamde loertoeter op de Mariaplaats te Utrecht in gebruik, waardoor met behulp van een stelsel van spiegels en lenzen een middeleeuws riool te bekijken is.
1988;9;17;De Utrechtse Vrijwillige Reddingsbrigade geeft ter gelegenheid van zijn 70-jarig bestaan een demonstratie op en in het Merwedekanaal.
1988;9;17;In de Muurhuizen te Amersfoort is een deel van de eerste stadsmuur uit de 13de eeuw blootgelegd.
1988;9;17;In Soest viert het echtpaar De Jong hun 75-jarig huwelijksfeest.
1988;9;21;Ir. G. J. M. Braks, minister van Landbouw en Visserij, opent officieel de al in april in gebruik genomen Rijks Agrarische School te Houten.
1988;9;22;Minister Brinkman start de bouw van het regionale verpleeghuis De Geinsche Hof te Nieuwegein.
1988;9;22;Op 57-jarige leeftijd overlijdt te Utrecht de beeldhouwster Willy Blees. Zij maakte o.a. één van de door vrouwen gemaakte beelden op de Utrechtse Maliebaan.
1988;9;23;In kasteel Groeneveld te Baarn wordt de tentoonstelling 'Begrensde Beweging' geopend met werk van de in 1972 overleden lithograaf en houtsnijder Maurits Cornelis Escher.
1988;9;28;Wethouder Pot ontvangt het eerst exemplaar van de door Monique Barnard samengestelde gids waarin alle in de stad Utrecht gesitueerde Rietveld-ontwerpen opgenomen zijn.
1988;10;2;In het gebouw van het Genootschap Kunstliefde aan de Nobelstraat te Utrecht wordt een overzichtstentoonstelling geopend van de in oktober 80 jaar worden Utrechtse kunstenaar Hans van Dokkum.
1988;10;4;In het Milieu Informatie Centrum Utrecht wordt de tentoonstelling "De Rijn, op en onderweg" geopend.
1988;10;7;De Stichting De Koepel verhuist van de Nachtegaalstraat in Utrecht naar het door de sterrenwacht van de Rijksuniversiteit Utrecht verlaten bolwerk Zonnenburg.
1988;10;7;Wethouder Van Willigenburg (VVD) en GVU directeur R. van Tetterode onthullen, ter gelegenheid van het 100-jarig bestaan van het vervoerbedrijf, op de Biltstraat in Utrecht een kopie van een antieke drinkfontein voor paarden.
1988;10;11;De laatste decaan van de op 1 september gesloten faculteit tandheelkunde van de Rijksuniversiteit Utrecht, prof. dr. G.J.M. Tonino overlijdt op 60-jarige leeftijd.
1988;10;11;Gedeputeerde T. Poortenaar-Sikkema geeft met het boren van de eerste paal, het startsein voor de bouw van het nieuwe Diakonessenhuis in Utrecht.
1988;10;14;J.M. van Vossen, die 23 jaar directeur was van de Gemeentelijke Muziekschool in Utrecht, maakt gebruik van de VUT regeling en ontvangt bij zijn afscheid de Speld van de stad Utrecht.
1988;10;14;Politieagenten in de stad beginnen hun acties tegen het regeringsbeleid met werkonderbrekingen en het niet uitschrijven van bekeuringen.
1988;10;15;Wegens zijn 100-jarig bestaan houdt de Hojelkazerne in Utrecht open huis.
1988;10;17;De reinigingsdienst van de gemeente Utrecht start in de wijk Zuilen met het gescheiden ophalen van afval.
1988;10;18;De herstellingsoorden Petrus Werthweijnstichting in Zeist en Oranjestein in Amerongen moeten sluiten aangezien het Rijk de subsidie heeft stop gezet.
1988;10;21;Mr. B. Kortenhoff-Nepperus ontvangt de zilveren stadsmedaille voor haar vele activiteiten in en voor de stad. waaronder het voorzitterschap van de manifestati Utrecht Middeleeuwse Kerkenstad.
1988;10;22;Op 85-jarige leeftijd overlijdt in Utrecht dr. J.G. van Cittert-Eymers, oud-directrice van het Utrechts Universiteitsmuseum.
1988;10;27;Gedeputeerde Poortenaar-Sikkema opent in het Provinciehuis in Utrecht een expo over kabouters. De werkstukken zijn gemaakt door verstandelijk gehandicapten uit heel Nederland.
1988;10;27;In de Stadsschouwburg wordt de eerste Utrechtse Vrijwilligersmarkt gehouden.
1988;10;27;De provincie Utrecht start met een Monumenten Invertarisatie Project om een overzicht te krijgen van de cultuur-historisch belangrijke gebouwen, complexen en gebieden uit de periode 1850-1940.
1988;10;28;Staatssecretaris mr. H.E. Koning van Financien opent in het Centraal Museum de expositie "Munt en beeldende kunst" van ontwerpen voor een 50 gulden muntstuk ter herdenking van de 100ste geboortedag van de architect Gerrit Rietveld.
1988;10;31;Kardinaal Simonis en wethouder Van Willigenburg (VVD) ontvangen de eerste exemplaren van het door dr. A. van Hulzen geschreven boek "Twee Wandelingen door de Middeleeuwse kerkenstad Utrecht"
1988;11;1;Prinses Margriet opent officieel het nieuwe centrum voor revalidatie De Hoogstraat te Utrecht. Al in mei was de verhuizing vanuit Leersum een feit geworden.
1988;11;2;Het Instituut Kunstzinnige Vorming en het Rijksmuseum Het Catharijneconvent presenteren in het kader van de manifestatie Utrecht Middeleeuwse Kerkenstad aan de Utrechtse onderwijskrachten een lespakket voor scholieren over de historie van de stad.
1988;11;3;In het Utrechts Universiteitsmuseum opent burgemeester Vos de tentoonstelling 'Het zieke kind in goede handen' ter gelegenheid van het 100-jarig bestaan van het Wilhelmina Kinderziekenhuis.
1988;11;3;Op 72-jarige leeftijd overlijdt te Hilversum prof. dr. H. van Praag, oud-hoogleraar in de parapsychologie aan de Rijksuniversiteit Utrecht.
1988;11;5;Wethouder Pot stelt de Utrechtse lokale radiozender Domroep in werking.
1988;11;9;Burgemeester Schreuder ontvangt het eerste exemplaar van Met het oog op de stad, een fotoboek over Amersfoort, bij elkaar gebracht door Arjeh Kalmann, Leo van der Horst en Bram Overeem.
1988;11;10;Bij de opening van de tentoonstelling 'Onder de rook van Utrecht' in het Utrechts Universiteitsmuseum wordt het gelijknamige boek van P. K. Smiesing en H. P. Brinkerink gepresenteerd.
1988;11;11;Bij haar afscheid als directrice van het Centraal Museum te Utrecht ontvangt drs. A. M. Janssens de zilveren stadsmedaille. Zij wordt opgevolgd door drs. K. M. T. Ex.
1988;11;14;Evenals in de rest van het land voert ook in Utrecht de politie massaal actie tegen de bezuinigingsplannen van het Kabinet.
1988;11;15;Staatssecretaris drs. N. J. Ginjaar-Maas van Onderwijs opent het nieuwe gebouw van de Christelijke Scholengemeenschap Abstede aan de Amerikalaan te Utrecht.
1988;11;18;Wethouder A. van Hassel van Economische Zaken slaat de eerste paal voor een modern bedrijvencomplex, Smart Business Park, aan de Kanaalweg te Utrecht.
1988;11;19;Dertig Utrechtse musea presenteren zich in de Jaarbeurs te Utrecht in het kader van de landelijke promotiecampagne Nederland museumland.
1988;11;21;Krakers bezetten het Utrechtse stadhuis om daar als protest tegen het huisvestingsbeleid, de uitvoering van de Leegstandswet en de dreigende ontruiming van het kraakpand Maliesingel 32 een 'schoonmaakactie' te houden.
1988;11;25;R. van Gelder krijgt bij zijn afscheid als algemeen directeur van de Koningin Wilhelmina Stichting, de stichting die de drie Utrechtse sociale werkplaatsen beheert, de Speld van de stad Utrecht. Hij wordt opgevolgd door D. van der Lee.
1988;11;25;Wethouder Van der Sluijs opent het verbouwde gedeelte van de verzorgingsflat Tuindorp-Oost te Utrecht.
1988;11;25;De gerenoveerde Noorderbrug over de Stadsbuitengracht te Utrecht wordt door wethouder Van Willigenburg (VVD) geopend.
1988;11;28;In de wijk Tuindorp-Oost gaat het eerste buurtpreventie-project in Utrecht van start met het doel vooral de kleine criminaliteit te verminderen.
1988;11;28;In de Crediet- en Effectenbank aan de Kromme Nieuwegracht te Utrecht wordt een tentoonstelling geopend over 'Zeven eeuwen farmacie in Utrecht' ter gelegenheid van het 55-jarig bestaan van de Utrechtse Apothekers Vereniging.
1988;11;29;Wethouder Andriessen-Brockman stort het eerste beton voor 88 woningen voor ouderen aan de Kruisstraat te Utrecht.
1988;12;1;Drs. C. van Draanen wordt muntmeester van 's Rijks Munt, hij volgt als zodanig ir. J. de Jong op die in december 1987 vertrok.
1988;12;2;Staatssecretaris Ginjaar-Maas, opent in Nieuwegein het nieuwe gebouw van de Stichting Centraal Onderwijs- en Administratiebureau.
1988;12;2;Wethouder Van Willigenburg (VVD) overhandigt de sleutel van de geheel gerestaureerde theekoepel in Park Oog in Al te Utrecht aan mevrouw W. Hol, directrice van de Openbare Bibliotheek, die de koepel gaat gebruiken als voorleesruimte.
1988;12;7;Op de Domproostenkapel van de Domkerk te Utrecht wordt een luidklok, de Johannes Baptista Minor geplaatst.
1988;12;9;Gedeputeerde Hoekstra en wethouder Van Willigenburg (VVD) maken een officieel begin met de restauratie van het middeleeuwse Pandhuis aan de Oudegracht te Utrecht, waar het Provinciaal en Gemeentelijk Depot voor Bodemvondsten gevestigd zal worden. Tijdens deze bijeenkomst ontvangen zij de Archeologische en Bouwhistorische Kroniek van de gemeente Utrecht over 1987 uit handen van gemeentelijk archeoloog drs. T. J. Hoekstra.
1988;12;10;Zes Utrechtse kunstenaars houden een open dag in hun gerestaureerde ateliers aan de Schalkwijkstraat.
1988;12;10;In de Stadsschouwburg te Utrecht wordt de landelijke manifestatie gehouden ter herdenking van het feit dat de Verenigde Naties veertig jaar geleden de Universele Verklaring van de Rechten van de Mens aannamen.
1988;12;14;De Utrechtse gemeenteraad neemt het besluit om de artikel-12 status aan te vragen, waarmee de gemeente zich onder toezicht van het Rijk stelt, om op die manier een hogere uitkering uit het gemeentefonds te krijgen.
1988;12;15;Minister Brinkman, opent in Slot Zeist een expositie van werkstukken van 30 beeldende kunstenaars, die zich hebben laten inspireren door 20 verschillende musea in de provincie Utrecht. Met deze tentoonstelling wordt tevens het museumjaar officieel afgesloten.
1988;12;16;In de omgeving van Utrecht valt regen met een zuurgraad gelijk aan die van azijn.
1988;12;16;Burgemeester Vos ontvangt uit handen van de commissaris van de Koningin een map met tekeningen van monumenten en stadsgezichten van de stad Utrecht, de zesde en laatste van een serie die de hele provincie bestrijkt.
1988;12;19;Minister Brinkman maakt in de Nicolaïkerk de prijswinnaars bekend van de prijsvraag, die door de Stichting Onderzoek Herinrichting Domplein Utrecht is uitgeschreven. De eerste prijs is voor 'Tracé régulateur' van de Rotterdammers ir. Stefan Gall, ir. Jaap van den Brunt en ir. Paul Achterberg.
1988;12;22;Met een rede getiteld Utrecht, centraal of marginaal? neemt jkvr. prof. dr. J. M. van Winter afscheid als hoogleraar in de geschiedenis van de Middeleeuwen van de Rijksuniversiteit Utrecht.
1988;12;23;Het gerestaureerde en tot 11 appartementen verbouwde voormalige stadskasteel Cranesteyn wordt door wethouder Van Willigenburg (VVD) officieel geopend.
1988;12;23;Bij haar afscheid als voorzitter van de Kring Utrecht van de Nederlandse Hartstichting ontvangt mevrouw A. Drijver-Huizinga de Maartenspenning van de stad Utrecht.
1988;12;24;Op 80-jarige leeftijd overlijdt te Utrecht de internist-cardioloog prof. dr. C . L . C , van Nieuwenhuizen. Hij wordt beschouwd als de grondlegger van de hartchirurgie in Nederland.
1988;12;31;In de gemeente Utrecht worden voor het eerst uit de ambtenaren de Vrouw en de Man van het Jaar gekozen. Het zijn Hennie van Tilburg, sinds juli van dit jaar chef van het bureau culturele zaken en Chris Swanink, chef van het bureau tekstverwerking.
