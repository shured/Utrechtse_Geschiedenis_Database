1941;1;1;Gedenking van het 50-jarig bestaan van de NV Kersbergen en Zonen.
1941;1;2;Gedenking van het 50-jarig bestaan van de NV Machinefabriek "Jaffa", voorheen Louis Smulders.
1941;1;5;H.L. Glinderman, voorzitter van de Christelijke Oranjevereniging en bestuurslid van verschillende Christelijke verenigingen, is overleden.
1941;1;11;Een nieuwe zaal bij de Raadskelder in gebruik genomen.
1941;1;11;Viering van het 450-jarig bestaan (op 13 januari) van het Gereformeerd Burgerweeshuis.
1941;1;14;C. Th. Klemann, oud-altist en 2e dirigent van het Utrechts Stedelijk Orkest, is in Gennep overleden.
1941;1;16;De Gemeenteraad besluit tot het beschikbaar stellen van een krediet van FL 96.970,= voor de verder aanleg van straten in het uitbreidingsplan "De Halve Maan".
1941;1;17;Dr. A. Lebret, oud-directeur van het Ingenieursbureau Nicoloai en Lebret, is overleden.
1941;1;22;Dr. C.D. Cramer oud-geneesheer-directeur van het Diakonessenhuis in Soest overleden.
1941;1;28;Dr. J.E. van der Meulen, oud-lector aan de Rijksuniversiteit, te Wapenveld overleden.
1941;1;31;Burgemeester en Wethouders doen mededeling aan de Gemeenteraad, dat van Rijkswege over de gehele stad verspreid een 20-tal overdekte schuilplaatsen gemaakt zullen worden.
1941;2;1;J.W.A. Haagen Smit, oud-scheikundige aan 's Rijks Munt, is overleden.
1941;2;3;De Kring Utrecht van het Verbond van Nederlanse Journalisten is opgericht en een bestuur is geinstalleerd.
1941;2;5;Eerste beschikbaarstelling van warme spijzen in de Centrale Keuken op het terrein van de Distributiedienst.
1941;2;5;Commissaris der koningin, Jhr. Mr. Dr. L.H.N. Bosch ridder van Rosenthal eervol ontslagen als zodanig vervangen door Ir. F.E. Muller.
1941;2;5;J.P.B. Biegelaar, eigenaar van de clichefabriek Biegelaar en Jansen, in Bilthoven overleden.
1941;2;6;De Gemeenteraad besluit tot intrekking van de bioscoopverordening.
1941;2;6;De Gemeenteraad besluit tot het beschikbaar stellen van grond aan de Jakerstraat en bij de gemeentelijke slachtplaats voor de bouw van centrale keukens.
1941;2;6;De Gemeenteraad besluit tot het toekennen van een tijdelijke toelage en tijdelijke verhoging van kindertoeslag aan lager bezoldigd gehuwd personeel.
1941;2;8;H.J. van den Berkhof, oud-wijkmeester van wijk II, waarvan de vroegere Wijk C deel uitmaakte, overleden.
1941;2;11;P. van Bekkum, toneelkapper (kapper van Beets), is overleden.
1941;2;12;2000 Inwoners van de Bremstraat en omgeving zijn geevacueerd in verband met het vermoeden van de aanwezigheid van een niet ontplofte bom. Het bomgat bleek later een oude kalkput te zijn.
1941;2;13;W.H. Beyen, oud-chef van de personeel-fondsen van de Nederlandse Spoorwegen, in Den Haag overleden.
1941;2;14;D.L. Hars, oud-directeur van de strafgevangenis, in Assen overleden.
1941;2;14;C.B. Wiesman, oud-aanemer, is overleden.
1941;2;15;Omroep-concert in Tivoli ten bate van Winterhulp Nederland.
1941;2;15;De drukkerij Hoonte bestaat 40 jaar. J. Douwes en G. Jongerius gedenken hun 40-jarig jubileum bij dit bedrijf.
1941;2;19;Het bestuur van de Christelijke kweekschool "Rehoboth" verleent aan J. van den Spek tegen 1 september eervol ontslag als directeur en benoemt Dr. L.W.G. Scholten als diens opvolger.
1941;2;21;Het nieuwe gebouw van de Rooms-katholieke huishoudschool aan de Nieuwegracht 56 wordt plechtig geopend.
1941;2;21;De burgemeester installeert de nieuw gevormde Armenraad. Mr. C. Veen is wederom als voorzitter gekozen.
1941;2;22;C. Knigge, oud-lid van de Provinciale Staten, in Wilnis overleden.
1941;2;26;De door de CPN georganiseerde staking in Amsterdam is overgeslagen naar Utrecht waar eerst Werkspoor en daarna Demka en Jaffa stil kwamen te liggen.
1941;2;27;De gisteren uitgeroepen staking is voorbij.
1941;2;27;De Gemeenteraad besluit tot stichting van een nieuw gebouw voor de gemeentelijke machinistenschool aan de Jutphaasscheweg.
1941;3;4;B. Renden legt het directeurschap van de Nieuwe muziekschool neer. F. van der Meyden blijft als enige directeur.
1941;3;4;Heck's Lunchroom in de Potterstraat, hoek oudegracht, is door brand verwoest.
1941;3;4;Mej. C. Voortman gehuldigd ter gelegenheid van haar 50-jarige medewerking aan het Utrechts Studententoneel.
1941;3;6;Het gezelschap "Utrechts Brandweer" bestaat 90 jaar.
1941;3;6;De nieuwe trap voor de personeelstunnel onder de perrons van het Centraal Station is in gebruik genomen.
1941;3;11;Tot de 20e maart is er de 44ste Jaarbeurs.
1941;3;13;Feestconcert gegeven door het Conservatorium-orkest ter gelegenheid van het 65-jarig bestaan van de muziekschool van de Maatschappij tot bevordering der toonkunst.
1941;3;16;Mej. Dr. S.E. Treffers neemt afscheid als predikante bij de Doopsgezinde gemeente.
1941;3;17;Gebakken vis, croquette, poffertjes, wafels, enz, zijn alle tegen inlevering van vet-bonnen verkrijgbaar gesteld.
1941;3;20;De Gemeenteraad besluit tot het verhuren van 11 hectare grond met bestemming tot volkstuintjes.
1941;3;20;De Gemeenteraad verleent een krediet van FL 100.800,= voor de aanleg van straten in het uitbreidingsplan "Krommerijn".
1941;3;20;De Gemeenteraad besluit tot demping van de Oosterstroom.
1941;3;21;Viering van het 40-jarig bestaan van de Voetbalvereniging "Door Oefening Sterk".
1941;3;22;Herdening van de stichting voor 100 jaren van het Evangelisch-Lutherse Wees-, Oudemannen- en Vrouwen-huis. Officele opening van het Evangelisch-Lutherse rusthuis "De Wartburg" aan de Hommel in Ouderijn.
1941;3;23;Jhr. Mr. J.W.M. Bosch van Oud-Amelisweerd, oud-president van de Arrondissementsrechtsbank, is overleden.
1941;3;30;A.J.C.F. Mittelmeyer gedenkt zijn 40-jarige werkzaamheid bij het Roomsch-Katholiek avond-nijverheidsonderwijs.
1941;3;31;J. Wallinga benoemd tot lid van de Gemeenteraad in de vacature ontstaan door het bedanken van Mevr. A.J. Wolthers-Arnolli.
1941;3;31;Aanvang van de opruiming van de oude rijtunnel in de Leidschen weg.
1941;4;2;Het "Selskip Fryslan" herdenkt zijn 40-jarig bestaan.
1941;4;2;Dr. H.A. Vermeulen, oud-lector aan de Veeartenijkundige Hogeschool en daarna aan de Rijksuniversiteit, in Maarn overleden.
1941;4;3;Ds.C. Veenhof, predikant van de Gereformeerde kerk, doet zijn intrede.
1941;4;4;Uitreiking van melk- en cacaokaarten.
1941;4;9;C.L. Tondu, oud-inspecteur bij het lager onderwijs, in Bilthoven overleden.
1941;4;10;E.P. van Dijk in Rhenen benoemd tot lid van de Provinciale Staten in de vacature onstaan door het bedanken van mevr. A.J. Wolthers-Arnolli.
1941;4;15;J.F. Mulders, inspecteur, herdenkt zijn 50-jarige jubileum bij de Nederlandse Spoorwegen.
1941;4;15;J.A. van Balen, oud-zendeling en leraar van de Utrechtse Zendingsvereniging, is overleden.
1941;4;16;P. Moltmaker, oud-lid van de Gemeenteraad en oud-voorzitter van de Personeelsraad van de Nederlandse Spoorwegen, is in Arnhem overleden.
1941;4;27;J. Jongerius, stichter en leider van een autobedrijf en handel, is overleden.
1941;4;28;Aanvang van de distributie van aardappelen.
1941;4;30;M. Bakker neemt afscheid als chef van de afdeling muntfabricage aan 's Rijks Munt.
1941;5;1;W. Wagenaar, arts, legt zijn praktijk neer.
1941;5;1;Dr. J.R. Slotemaker de Bruine, oud-predikant van de Nederduits Hervormde gemeente en oud-hoogleraar aan de Rijksuniversiteit, in Wassenaar overleden.
1941;5;1;Het bureau der Rijks Zuivelinspectie bestaat 40 jaar.
1941;5;1;Tot de 7e is de uitreiking van aardappelkaarten.
1941;5;2;A.J.W. Ente, directeur van de strafgevangenis, is overleden.
1941;5;8;Dr. C. Winkler, oud-hoogleraar aan de Rijksuniversiteit, in Hoog-Soeren overleden.
1941;5;9;Ds. P.J. Mink, arts, is overleden.
1941;5;10;Uitbreiding van de distributie van schoeisel over meer soorten.
1941;5;12;Uitreiking van persoonsbewijzen (tot 30 oktober van dit jaar).
1941;5;13;C. Brandt, oud-leraar aan de Gemeentelijke Hogere Burgerschool met handelsschool, is overleden.
1941;5;13;W.P. Ingenegeren, directeur-generaal van de NV Levensverzekering Maatschappij "Utrecht", in De Bilt overleden.
1941;5;13;Onthulling van een monument op het graf van Corn. Galesloot op de Roomsch-katholieke begraafplaats.
1941;5;15;Mr. G.J. Couvee is geinstalleerd als rechter bij de Arrondissementsrechtsbank.
1941;5;17;De uitbreiding van het kantoorgebouw van de Maatschappij Ziekenfonds "Voorzorg en hulp bij Ziekte" aan de Catharijnesingel, is officieel in gebruik genomen.
1941;5;20;Dr. L.S. Ornstein, oud-hoogleraar aan de Rijksuniversiteit, is overleden.
1941;5;21;Eerste zitting van de economische rechter (Mr. G. J. Couvee).
1941;5;23;J. Kruseman Aretz, litt. docts. en bekend geleerde, is overleden.
1941;5;24;Onthulling van een bronzen gedenkplaat in de gevel van het huis van Cath. van Rennis, Brigittenstraat 1. Op hetzelfde moment begint de tentoonstelling over Cath. van Rennes in het Centraal Museum (tot 2 juni).
1941;5;25;K.J.C. Rab, notaris, is overleden.
1941;5;26;Het nieuwe gebouw van Galeries Modernes aan de Lange Viestraat, hoek Oudegracht, is officieel geopend.
1941;5;26;tentoonstelling betreffende het toneel en de schouwburg in de foyer van de schouwburg (aan het Vredenburg) ter gelegenheid van de sluiting van het gebouw (tot 2 juni).
1941;5;27;Sluitings-voorstelling in de oude schouwburg. E.W. de Blauw neemt afscheid als directeur.
1941;5;29;Ds. M. de Goede, predikent van de Gereformeerde kerk, doet zijn intrede.
1941;5;29;De Gemeenteraad besluit tot verbreding van de singelgracht bij de nieuwe schouwburg.
1941;5;30;K.F. Borgers neemt afscheid als bode van de Vereniging tot verbetering van armenzorg.
1941;6;1;Dr. A.K.M. Noyens, hoogleraar aan de Rijksuniversiteit, is overleden.
1941;6;1;De Algemene Stationsboekhandel, gevestigd in Utrecht, is 50 jaar aanwezig op de stations van de Nederlandse Spoorwegen.
1941;6;3;F.J. van Straaten gedenkt zijn 40-jarige werkzaamheid bij de Eerste Nederlandsche Asphaltfabriek voorheen Gürtzen en Van Straten.
1941;6;6;D.A. van der Sanden, vice-consul van Noorwegen, is overleden.
1941;6;8;Eerste bokswedstrijd na opheffing van het verbod gehouden in Tivoli.
1941;6;9;Het neergaande spoor van de verhoogde spoorbaan Utrecht-Amersfoort is in bedrijf gesteld.
1941;6;9;C. Th. Bosselaar, gedurende 50 jaar voorzitter van de afdeling Utrecht van de Nederlandschen kappersbond, is overleden.
1941;6;11;Het nieuwe kerkorgel in het Stads- en Academisch ziekenhuis is feestelijk in gebruik genomen.
1941;6;12;Mr. W.H. Verloop benoemd tot directeur-generaal van de NV Levensverzekering Maatschappij "Utrecht" in de vacature ontstaan door het overlijden van W.P. Ingenegeren.
1941;6;16;De oud-fotograaf G. Jochmann is in Zeist overleden.
1941;6;16;Tot de 21e is de sportweek, georganiseerd door het Utrechts Studentencorps ter gelegenheid van het 61e lustrum van de Rijksuniversiteit.
1941;6;17;Dr. J. Wagenaar, componist en oud-directeur van de muziekschool en van de zangafdeling van de afdeling Utrecht van de Maatschappij tot bevordering der Toonkunst, is in Den Haag overleden.
1941;6;19;De Gemeenteraad besluit tot onttrekking aan het openbaar verkeer het Rozenhofje aan de Maliesingel.
1941;6;19;De Gemeenteraad stelt de Verordening tot heffing en invordering van een baatbelasting in verband met de uitvoering van de spoorwegwerken vast.
1941;6;21;Opening van de tentoonstelling van werken van oude Utrechtse meesters in het Centraal Museum, ter gelegenheid van het 61e lustrum van de Rijksuniversiteit.
1941;6;21;Vandaag en morgen zijn de jubileum-roeiwedstrijden op het Amsterdam-Rijnkanaal ter gelegenheid van het 50-jarig bestaan (op 5 okt. 1940) van de Utrechtse Studenten-roeivereniging Triton.
1941;6;22;Tot de 24e zijn de toneelvoorstellingen ter gelegenheid van het 61e lustrum van de Rijksuniversiteit in de daarvoor alsnog beschikbaar gestelde oude schouwburg.
1941;6;23;De Secretratis-Generaal van het Departement van Opvoeding, Wetenschap en Cultuurbescherming trekt de toestemming tot sloop van de Geertekerk voorlopig in.
1941;6;23;Eerste algemene Universiteitsdag.
1941;6;24;Plechtige gedenking van het 305-jarig bestaan van de Hogeschool.
1941;6;24;De Roomsch-Katholieke noodkerk van de nieuwe St. Jansparochie op Hoog-Raven is ingewijd.
1941;6;25;Vandaag en morgen zijn de gobelins (wandtapijten), door het Utrechts Universiteitsfonds ter gelegenheid van het 60e lustrum van de Rijksuniversiteit geschonken, in het groot-auditorium te bezichtingen.
1941;6;25;Tot de 28e zijn de avond-vierdaagsche wandeltochten georganiseerd door de Stichtsch-Gooischen- en de Nederlandschen Wandelsportbond.
1941;6;27;Mr. Dr. K.H. Beyen, oud-secretraris van de Nederlandse Spoorwegen, is in Den Haag overleden.
1941;6;28;Mr. P.C.J. Cremer, advocaat en lid van de Voogdijraad, is overleden.
1941;6;30;G.H.J. van Spanje en Mr. G.A. Diepenhorst zijn ontslagen als leden van het Provinciaal Bestuur en vervangen door L.C. Schouten en Mr. J. Frima.
1941;7;5;Viering van het 40-jarig bestaan van de Utrechtschen Provincialen Voetbalbond.
1941;7;9;Regenten van de Verenigde Gods- en gasthuizen berichten aan B&W af te zien van het plan-Ir A. van der Steur voor de bouw van het Geertehof ter plaatse van de af te breken Geertekerk.
1941;7;10;De Gemeenteraad stelt een krediet beschikbaar voor het geven van openlicht-voorstellingen in het Kloosterhof van de Dom.
1941;7;10;De Gemeenteraad besluit tot opheffing van de openbare lagere school aan de Joan Röellstraat 2.
1941;7;10;Inlevering van metalen op grond van de Verordening betreffende de inlevering van metalen van 18 juni, no 108 (tot 12 augustus).
1941;7;11;De Utrechtsche Studenten-corpora zijn ontbonden.
1941;7;11;Mr. Dr. J.H. Jonckers Nieboer, oud-hoofdinspecteur voor de rechtszaken bij de Nederlandse Spoorwegen, is in Bilthoven overleden.
1941;7;13;Gedenking in de Dom van het 125-jarig bestaan van de Utrechtsche Bijbelvereniging.
1941;7;14;Aanvang van de distributie van jam, stroop, honing, enz.
1941;7;16;Openluchtvoorstellingen in de Kloosterhof van de Dom, onder leiding van A. Hooykaas (tot 2 augustus).
1941;7;17;Ds. R.J. van der Meulen, predikant van de Gereformeerde kerk, doet zijn intrede.
1941;7;18;T. Sprey neemt afscheid als hoofd van de Gereformeerde Burgerschool aan de Plompetorengracht. A. Bergsma is als zijn opvolger geinstalleerd.
1941;7;19;Mr. J.A. Nederburgh, oud-bijzonder hoogleraar aan de Rijksuniversiteit, is in Den Haag overleden.
1941;7;19;Aanvang van de Duitse V-actie. Huizen, muren en straten zijn beplakt en beschilderd met leuzen.
1941;7;19;De verkoop van thee is gestaakt wegens gebrek aan voorraad.
1941;7;31;De Gemeenteraad besluit de Andersenschool aan het Ondiep 63 te verplaatsen naar het gebouw van de opgeheven openbare lagere school aan de Joan Röellstraat 2.
1941;8;1;Ir. Ch. H.J. Driessen, hoofd-ingenieur en chef van de dienst Weg en Werken, gedenkt zijn 40-jarige werkzaamheid bij de Nederlandse Spoorwegen.
1941;8;3;Th. Lans, hoofd van de Dr. De Visserschool, is overleden.
1941;8;6;J. Middelweerd gedenkt zijn 40-jarige werkzaamheid bij de NV Utrechtsche machinefabriek Frans Smulders aan de Croeselaan.
1941;8;11;Ds. J.A.F.A. Stellwag, predikant van de Evangelische-Lutherse gemeente, is gehuldigd ter gelegenheid van zijn 40-jarige ambtsbediening.
1941;8;15;Ir. P. Labrijn, hoofd-ingenieur en adjuct-chef van de dienst van Tractie en materieel, bedenkt zijn 40-jarige werkzaamheid bij de Nederlandse Spoorwegen.
1941;8;15;Pater F.F. Bergman O.P. gedenkt zijn 40-jarige priesterschap.
1941;8;17;W. van Lankeren, directeur van de Bank voor belegging en administratie en rijkstaxateur voor onroerende goederen in Utrecht, is in Loosdrecht overleden.
1941;8;17;D.P. Bierens de Haan, zenuwarts, is in Drieberg overleden.
1941;8;20;De Nederlandse Zondagschoolvereniging gedenkt haar 75-jarig bestaan.
1941;8;24;Dr. J.H.F. Kohlbrugge, oud-hoogleraar aan de Rijksuniversiteit, is in Zeist overleden.
1941;8;28;Burgemeester Ter Pelkwijk is weer beter en zit de laatste vergadering van de Gemeenteraad voor.
1941;8;28;De Gemeenteraad besluit tot het verlenen van een subsidie van FL 25.000,= aan de NV Stadsschouwburg.
1941;8;29;J.C.F. Bouman neemt afscheid als ontvanger der directe belastingen.
1941;8;30;C.A. Klinkenberg neemt afscheid als inspecteur der invoerrechten en accijnsen.
1941;8;30;Het nieuwe gebouw van de Nieuwe Muziekschool aan de Parkstraat 16 is officieel in gebruik genomen.
1941;9;1;De werkzaamheden van de gemeenteraden, colleges van B&W en de Provinciale Staten worden ingevolgde een op 11 augustus uitgevaardigde Verordening, vanaf nu waargenomen door de burgemeesters en de Commissaris der Provncie.
1941;9;2;Het meisjes-lyceum door het vervallen van de gymnasiale afdeling volgens raadsbesluit van 20 december 1935 weer gewijzigd in een hogere burgerschool.
1941;9;2;Burgemeester Ter Pelkwijk besluit tot aankoop van gronden bij het Zandpad en de Jagerskade, bestemd voor de plaatsing van de zuiveringsinrichting van rioolwater.
1941;9;3;De borstbeelden van Dr. F.H. Fentener van Vlissingen en burgemeetesr Ter Pelkwijk, zijn geplaatst in de hal van de nieuwe schouwburg en door een uit de burgerij gevormd comite overgedragen aan de Raad van Bestuur van de Schouwburg.
1941;9;4;Dr. A. Hengeveld neemt afscheid als rector van het Christelijke gymnasium.
1941;9;4;De nieuwe schouwburg aan het Lucasbolwerk is in gebruik genomen.
1941;9;5;S. Been, firmant van de houthandel firma Koker, is overleden.
1941;9;6;Postzegeltentoonstelling in Tivoli ter gelegenheid van het 35-jarig bestaan van de Utrechtsche Philatelistenvereniging.
1941;9;8;De verhoogde spoorbaan Utrecht-Amersfoort is nu helemaal in gebruik.
1941;9;9;Tot de 18e van deze maand is de 45e Jaarbeurs.
1941;9;15;Dr. H.R. Kruyt geeft verslag van de lotgevallen van de Rijksuniversiteit en treedt af als rector magnificus.
1941;9;16;L. van Vuuren neemt in zijn hoedanigheid van oudste in jaren der assessoren de werkzaamheden van rector magnificus op zich.
1941;9;19;Burgemeester Ter Pelkwijk besluit tot verkoop van de opstal van de oude schouwburg aan het bestuur van de Volks-Universiteit te Rotterdam voor FL 10.187,=.
1941;9;23;De NV Instrumentenfabriek "Marius" aan de Ganzenmarkt, bestaat 75 jaar.
1941;9;23;De burgemeester besluit tot aankoop van Lange Nieuwstraat 2, om het te verhuren aan de afdeling Utrecht van de Maatschappij tot bevordering der toonkunst voor haar Muziekschool.
1941;9;24;Zuster C.M. Haspels neemt afscheid als besturende zuster van het Diakonessenhuis. Zuster M.J.C. Besselaar is geinstalleerd als haar opvolgster.
1941;9;27;H.P. Geerke, oud-hoogleraar aan de Gemeentelijke Hogere Burgerschool met handelsschool en 5-jarigen cursus, is in Den Haag overleden.
1941;9;28;Dr. H. Jakob, oud-hoogleraar aan de Rijksuniversiteit, is in Nordingen overleden.
1941;9;28;H.A.J. Boudier neemt afscheid als directeur van het post- en telegraafkantoor.
1941;9;30;Mr. J.G.L. Nolst Trenite, schrijver van twee lustrumspelen voor het Utrechtsch Studentencorps, is in Rotterdam overleden.
1941;10;1;Dr. H.F.G. Kaiser, lid van de firma G. Grive, loodwit-fabrikanten, is overleden.
1941;10;1;D. Roosenschoon, oud-voorzitter van de Utrechtse Smedenpatroonsvereniging en directeur van de NV Zimmerman & Co., is in Bilthoven overleden.
1941;10;4;H.F. Grondijs, oud-kommandeur bij het brandwezen, is overleden.
1941;10;4;Indische tentoonstelling in het Centraal Museum ter gelegenheid van het 25-jarig bestaan het het Utrechtsch museum ten dienste van het onderwijs (tot 2 november).
1941;10;6;A. ten Have neemt afscheid als directeur van de Ned. Herv. Burgerschool aan het Domplein.
1941;10;10;W.B. Engelbrecht is benoemd tot Commissaris der Provincie Utrecht in plaats van Ir. F.E. Muller, die benoemd is tot burgemeester van Rotterdam.
1941;10;11;Alle zaterdagmarkten zijn samengebracht op het Vredenburg.
1941;10;11;Tot de 26e is de tentoonstelling van schilderijen van de Geertekerk en het Geertekerkhof, ingezonden door leden van het genootschap "Kunstliefde" op de door dit genootschap uitgeschreven prijsvraag.
1941;10;12;Ds. A.W. Brink is bevestigd in zijn dienst voor de arbeid onder de studenten van de Rijksuniversiteit vanwege de Ned. Herv. gemeente.
1941;10;17;De burgemeester stelt een krediet van FL 109.575,= beschikbaar voor de verdere straataanleg in het uitbreidingsplan "Pijlsweerd".
1941;10;20;Ir. F.E. Muller neemt afscheid als Commissaris der provincie wegens aanvaarding van het burgemeesterschap van Rotterdam.
1941;10;24;Openstelling van de gelegenheid tot het ruilen van kinderschoenen.
1941;10;29;De Utrechtse rederijkerskamer "Nicolaas Beets" gedenkt haar 30-jarig bestaan (op 10 april).
1941;10;29;De burgemeester besluit aan de afdeling Utrecht van de Nederlandse Vereniging voor luchtbescherming een subsidie te verlenen van FL 9.000,=.
1941;11;1;F.C.J. Nagtegaal, procuratiehouder bij de NV Twentsche bank, gedenkt zijn 50-jarige werkzaamheid bij de Stichtsche bank, in 1918 verenigd met de Twentsche bank.
1941;11;2;J. Flentge, steenhouwer bij de herstellingswerkzaamheden van de Domtoren, is overleden.
1941;11;2;Aanvang van de distributie van cacaopoeder voor kinderen beneden de 14 jaar.
1941;11;4;De burgemeester besluit tot onttrekking aan het openbaar verkeer van het zuidelijke gedeelte van het Paddemoes.
1941;11;8;M. J. Gijlswijk O.P. oud-pastoor van de St. Dominicusparochie, is in Nijmegen overleden.
1941;11;12;Kanunnik E. Lagerwey is door Mgr. Dr. A. Rinkel, Aartsbisschop van de Oud-Katholieke kerk, gewijd tot bisschop van Deventer.
1941;11;13;Dr. H. Waller, oud-directeur van de Rotterdamse bankvereniging, kantoor Utrecht en oud-voorzitter van de vereniging Oud-Utrecht, is in Bilthoven overleden.
1941;11;17;De NSB-er W.B. Engelbrecht is geinstalleerd als Commissaris van de provincie Utrecht
1941;11;19;Dr. J.H. Schuurmans Stekhoven, oud-inspecteur van het staatstoezicht op krankzinnigen en krankzinnigen-gestichten, is overleden.
1941;11;19;De burgemeester besluit tot het treffen van voorlopige voorzieningen aan de zuidelijke Nicolaastoren wegens dreigend verval.
1941;11;21;C.J.A. van Ginkel, oud-chef van de centrale controle bij de Nederlandse Spoorwegen, is overleden.
1941;11;23;De organist van de Nicolaaskerk, J.C. de Lange, is tijdens de morgendienst overleden.
1941;11;25;De burgemeester besluit, dat instandhouding van de openbare lagere school aan de Abstederdijk 142 en van de openbare lagere school aan de Waterstraat 52 wordt gevorderd ingevolge art. 9 der lager onderwijs-wet 1920.
1941;11;25;De burgemeester besluit tot het verlenen van een extra subsidie aan de besturen van verschillende frobelscholen wegens derving van inkomsten, vroeger verkregen door het houden van collecten en bazars.
1941;11;28;G.W. Stroink, voorzitter van het bestuur van de Cooperatieve Centrale Raiffeisenbank, is overleden.
1941;11;29;E.G. van den Bovenkamp neemt afscheid als directeur der directe belastingen.
1941;11;29;J.A. Petter neemt afscheid als hoofd-inspecteur der directe belastingen.
1941;11;29;H.L. Boerrigter, voorzitter van de Leo-vereniging, is overleden.
1941;11;30;Feestelijke gedenking van het 60-jarig bestaan van het zangkoor Cantate Domino van de St. Martinus-parochie aan de Oudegracht.
1941;12;1;Viering van het 80-jarig bestaan van de Utrechtse rijkerskamer Jan van Beers.
1941;12;2;P.A. Lens, oud-notaris, is in Haarlem overleden.
1941;12;2;Dr. A. Vrijburg, dierenarts en oud-conservator van de Veeartsenijkundige Hogeschool, in Den Haag overleden.
1941;12;8;D.W.J. Ruiters, oud-hoofd van de school aan de Deken Roesstraat, is overleden.
1941;12;9;R. Veenstra, oud-directeur van het Stads-kindertehuis, in Oudenrijn overleden.
1941;12;10;H.A. van de Weerd, gedenkt zijn 50-jarige werkzaamheid als apotheker.
1941;12;15;De Aartsbisschop verleent met 1 janauri eervol ontslag aan J.E. Brom als conservator van het Aartsbisschoppelijk Museum en benoemt Drs. J.J.M. Timmers als opvolger.
1941;12;18;J.C. Ras, oud-eerste concertmeester van het Utrechts Stedelijke Orkest, in Rijswijk overleden.
1941;12;20;W. de Ru is gehuldigd ter gelegenheid van zijn 50-jarige werkzaamheid als secretratis van de rederijkerskamer Moliere.
1941;12;22;P.M. Rinkel, oud-pastoor van de Oud-Katholieke gemeente van de H. Jacobus in de Bemuurde Weerd en kanunnik van het Metropolitaan kapittel in Utrecht, is in De Bilt overleden.
1941;12;24;Dr. J.C. Franken, hoogleraar aan de Rijksuniversiteit, is overleden.
1941;12;26;De 100ste voorstellingen gegeven in de nieuwe schouwburg.
1941;12;27;Prof. Dr. H.C.L.E. Berger neemt afscheid als veterinair hoofd-inspecteur van de volksgezondheid en als directeur van de Veeartsenijkundigen dienst.
1941;12;31;Het filiaal van de Openbare leeszaal aan de Sweelinckstraat is opgeheven.
