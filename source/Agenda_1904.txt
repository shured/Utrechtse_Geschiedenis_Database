1904;11;22;De Remonstrantse predikant dr. H.I. Oort herdenkt in een rede de geboortedag van de "dichter van de weemoed en hoop", Petrus Augustus de Génestet (Amsterdam, 21 november 1829 – Rozendaal, 2 juli 1861).
1904;11;24;De heer J.M.C Kooiman, controleur van 's Rijks Munt, overleden. Hij werd slechts 32 jaar en overleed na een hevig en kort ziekbed.
1904;11;24;B&W delen mee dat de Grote Eligensteeg tussen 24 en 30 november voor het verkeer van rijtuigen zal zijn afgesloten.
1904;12;10;De Utrechtse Gymnastiek- en Schermvereniging Bato geeft deze zaterdag haar jaarlijkse uitvoering in Tivoli.
