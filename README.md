De hele database is online te raadplegen via => http://opendata.shured.nl/Gebeurtenissen/

Source directory contains all the source data in csv form

Script will output an utrecht.db in sqlite3 format.

Maak een sqlite3 db met de naam utrecht.db en maak tabel aan via onderstaand statement

CREATE TABLE agenda (
    "Jaar" INTEGER,
    "Maand" INTEGER,
    "Dag" INTEGER,
    "Omschrijving" TEXT
);

Het script vuldb.pl zal door alle gevonden Agenda_*.txt heenlopen en deze aan de DB toevoegen.

Het script vul.pl neemt 1 Agenda_<jaar>.txt bestand als input en update dit jaar in de db door eerst alle regels weg te gooien en opnieuw aan te maken.

